﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WcfEngine
{
    public class ClientesPreview
    {
        public string nomjefe { set; get; }
        public string nomsupervisor { set; get; }
        public string NCategoria { set; get; }
        public string ncedis { set; get; }
        public string idruta { set; get; }
        public string nruta { set; get; }
        public string idcliente { set; get; }
        public string ncliente { set; get; }
        public string direccion { set; get; }
        public string ngiro { set; get; }
        public string ntamanio { set; get; }
        public string npotencial { set; get; }
        public string cajas { set; get; }
        public string skus { set; get; }
        public string ncanalrtm { get; set; }
        public string AccountId { get; set; }
    }
}