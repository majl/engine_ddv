﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Xml;
using System.Configuration;

namespace WcfEngine
{
    public class LP
    {
        public LP()
        {

        }

        public DataSet Simulador(XmlDocument xDoc, string oportguid, string lpguid, double? compromiso)
        {

            // motor clientes
            DataSet dsrulecondition = new DataSet();

            Engine engine = new Engine();
            dsrulecondition = engine.ExecutePreview(xDoc);

            DataTable dt = null;


            string sql = string.Empty;
            string cadcon = string.Empty;
            sql = "create table #list_prods(  " + Environment.NewLine +
            "	lpid uniqueidentifier, " + Environment.NewLine +
            "	lpname varchar(25),  " + Environment.NewLine +
            "	skuid uniqueidentifier,  " + Environment.NewLine +
            "	itemid varchar(40),  " + Environment.NewLine +
            "	codope int,  " + Environment.NewLine +
            "	precio float, " + Environment.NewLine +
            "	precionue float,  " + Environment.NewLine +
            "	aplica bit)			 " + Environment.NewLine + Environment.NewLine;

            sql += "  create table #clientescedis( " + Environment.NewLine +
   " cuc int, " + Environment.NewLine +
   " nombre  varchar(4000),  " + Environment.NewLine +
   " idcliente uniqueidentifier,  " + Environment.NewLine +
   " idcedis uniqueidentifier,   " + Environment.NewLine +
   " cedis int,    " + Environment.NewLine +
   " idgiro uniqueidentifier,    " + Environment.NewLine +
   " idctacve uniqueidentifier,   " + Environment.NewLine +
   " cedisname varchar(500),    " + Environment.NewLine +
   " ctacve varchar(500),    " + Environment.NewLine +
   " giro varchar(500)   " + Environment.NewLine +
  ") " + Environment.NewLine + Environment.NewLine;

            sql += " /*insert into #clientescedis " + Environment.NewLine +
 "select cast(cli.ope_idcliente as int) as cuc,    " + Environment.NewLine +
 "  cast(cli.name as varchar(4000))as nombre,   " + Environment.NewLine +
 "  cli.AccountId as idcliente,   " + Environment.NewLine +
 "  cedis.Ope_cedisId  as idcedis,   " + Environment.NewLine +
 "  cast(cedis.ope_idcedis as int) as cedis,    " + Environment.NewLine +
 "  g.Ope_girosId as idgiro,   " + Environment.NewLine +
 "  cc.Ope_cuentasclaveId as idctacve,   " + Environment.NewLine +
 "  cast(cedis.ope_name as varchar(500)) as cedisname,    " + Environment.NewLine +
 "  cast(cc.ope_name as varchar(500)) as ctacve,    " + Environment.NewLine +
 "  cast(g.ope_name as varchar(500)) as giro   " + Environment.NewLine +
 "from  Account cli    " + Environment.NewLine +
 "inner join Ope_asignacion asig  on cli.accountid = asig.ope_clienteid   " + Environment.NewLine +
 "inner join Ope_rutas rutas on asig.ope_rutaid = rutas.Ope_rutasId   " + Environment.NewLine +
 "       and rutas.statecode = 0 /*activas*/   " + Environment.NewLine +
 "inner join ope_cedis cedis on rutas.Ope_CedisId = cedis.Ope_cedisId " + Environment.NewLine +
 "left join ope_subcanalisscom sis on cli.ope_subcanalid = sis.ope_subcanalisscomId  " + Environment.NewLine +
 "left join ope_giros g on sis.ope_giroid = g.ope_girosid   " + Environment.NewLine +
 "left join Ope_cuentasclave cc on cli.Ope_CuentaClaveId = cc.Ope_cuentasclaveId   " + Environment.NewLine +
 "where  asig.statecode = 0 /*las activas*/  " + Environment.NewLine +
 "and cedis.Ope_idcedis = 31 */" + Environment.NewLine + Environment.NewLine;


            sql += "INSERT INTO #list_prods " + Environment.NewLine +
                    "select lpi.ope_listadeprecios, lpi.ope_listadepreciosname, lpi.ope_skuid, " + Environment.NewLine +
                    "       inv.ope_itemid, inv.ope_cxcodope, lpi.ope_precio, lpi.ope_precionue, lpi.ope_aplica " + Environment.NewLine +
                    "  from ope_listaprecioscrmitems lpi " + Environment.NewLine +
                    "inner join ope_inventable inv on lpi.ope_skuid = inv.ope_inventableid " + Environment.NewLine +
                    " where ope_listadeprecios = '" + lpguid + "'" + Environment.NewLine + Environment.NewLine;


            sql += "select tdescuento = case when lddet.ope_skuid is not null " + Environment.NewLine +
                "		 then 'SKU'  " + Environment.NewLine +
                "		 else (case when lddet.ope_Empaqueid is not null  " + Environment.NewLine +
                "					then 'Empaque'  " + Environment.NewLine +
                "					else 'Marca' end)  " + Environment.NewLine +
                "		 end, " + Environment.NewLine +
                "lddet.ope_marcaid, lddet.ope_marcaidname, lddet.ope_Empaqueid, lddet.ope_Empaqueidname, lddet.ope_Skuidname,  " + Environment.NewLine +
                "inv.ope_cxcodope, lddet.ope_oportunidadidname, lddet.ope_skuid, " + Environment.NewLine +
                "lddet.ope_pesos, lddet.ope_porcentaje, lddet.ope_oportunidadid " + Environment.NewLine +
                " into #descuentos " + Environment.NewLine +
                "from ope_listadedescuentoproductos lddet " + Environment.NewLine +
                "left join ope_inventable inv on lddet.ope_Skuid = inv.ope_inventableid " + Environment.NewLine +
                " where lddet.ope_oportunidadid = '" + oportguid + "' " + Environment.NewLine +
                "   and lddet.statecode = 0 " + Environment.NewLine + Environment.NewLine;

            sql += "select lp.*,inv.ope_marcaid, inv.ope_marcaidname " + Environment.NewLine +
                  "into #prods " + Environment.NewLine +
                  "from #list_prods lp " + Environment.NewLine +
                  "left join ope_inventable inv on lp.skuid = inv.ope_inventableid " + Environment.NewLine + Environment.NewLine;

            sql += "select lp.*, tdescuento = '                 ', ope_oportunidadidname = '                            ', " + Environment.NewLine +
                   " ope_pesos = 0, ope_porcentaje = 0, ope_oportunidadid='', aplicadesc = 0  " + Environment.NewLine +
                  "  into  #final " + Environment.NewLine +
                  "  from  #prods lp /*#descuentos  d " + Environment.NewLine +
                  "inner join #prods lp on d.ope_marcaid = lp.ope_marcaid " + Environment.NewLine +
                  "where d.ope_cxcodope is null " + Environment.NewLine +
                  "  and d.ope_empaqueid is null*/" + Environment.NewLine + Environment.NewLine;

            sql += "/* update por marca*/" + Environment.NewLine +
"update #final " + Environment.NewLine +
"   set tdescuento = d.tdescuento," + Environment.NewLine +
"       ope_oportunidadidname = d.ope_oportunidadidname, " + Environment.NewLine +
"	   ope_pesos = d.ope_pesos," + Environment.NewLine +
"	   ope_porcentaje = d.ope_porcentaje," + Environment.NewLine +
"	   aplicadesc = 1" + Environment.NewLine +
"  from #descuentos  d" + Environment.NewLine +
" where d.ope_marcaid = #final.ope_marcaid" + Environment.NewLine +
"   and d.ope_cxcodope is null " + Environment.NewLine +
"   and d.ope_empaqueid is null" + Environment.NewLine + Environment.NewLine;

            sql += "/*desc por marca-empaque " + Environment.NewLine +
                   "update #final " + Environment.NewLine +
    "set tdescuento = d.tdescuento, " + Environment.NewLine +
     "   ope_oportunidadidname = d.ope_oportunidadidname," + Environment.NewLine +
     "   ope_pesos = d.ope_pesos, " + Environment.NewLine +
     "   ope_porcentaje = d.ope_porcentaje," + Environment.NewLine +
     "   aplicadesc = 1 " + Environment.NewLine +
   " from #descuentos  d" + Environment.NewLine +
  " where d.ope_marcaid = #final.ope_marcaid" + Environment.NewLine +
  "  and d.ope_empaqueid = #final.ope_cxempaqueid" + Environment.NewLine +
  "  and d.ope_empaqueid is not null  " + Environment.NewLine +
  "  and d.ope_cxcodope is null */" + Environment.NewLine + Environment.NewLine +

                   "/*desc por sku*/ " + Environment.NewLine +
                   "update #final " + Environment.NewLine +
                   "   set tdescuento = d.tdescuento, ope_oportunidadidname = d.ope_oportunidadidname," + Environment.NewLine +
                   "	   ope_pesos = d.ope_pesos, " + Environment.NewLine +
                   "	   ope_porcentaje = d.ope_porcentaje, aplicadesc = 1 " + Environment.NewLine +
                   "  from #descuentos  d " + Environment.NewLine +
                   " where convert(int,#final.codope) = convert(int,d.ope_cxcodope) " + Environment.NewLine +
                   "   and d.ope_cxcodope is not null  " + Environment.NewLine + Environment.NewLine;

            sql += " select * from #final where aplicadesc = 1";

            /* Conexion a CRM */
            //SQLManejador conexion = new SQLManejador("Data Source=srvbdcrm2011.mdaote.bepensa.local;Initial Catalog=BebidasDesLP_MSCRM;uid=usr_spbebidasdes;pwd=bebidasdes");
            cadcon = ConfigurationManager.ConnectionStrings["BebidasDes_Connection"].ConnectionString;
            SQLManejador conexion = new SQLManejador(cadcon);
            
            if (conexion.AbrirConexion())
            {
                conexion.Instruccion = sql;
                dt = conexion.EjecutarConsulta(new DataTable("lista"));
            }

            dsrulecondition.Tables.Add(dt);

            sql = "declare @dt_ini smalldatetime, @dt_fin smalldatetime, @dt_aux smalldatetime " + Environment.NewLine +
                  "declare @domingos int, @dias int   " + Environment.NewLine + Environment.NewLine +

                  "select @dt_ini = convert(smalldatetime,convert(char(10),ope_fechainicial,101)), " + Environment.NewLine +
                  "	   @dt_fin = convert(smalldatetime,convert(char(10),ope_fechafin,101)) " + Environment.NewLine +
                  "  from ope_oportunidad " + Environment.NewLine +
                  " where ope_oportunidadid = '" + oportguid + "' " + Environment.NewLine + Environment.NewLine +

                  "select      @dt_aux     =     @dt_ini   " + Environment.NewLine +
                  "select      @domingos   =     0   " + Environment.NewLine + Environment.NewLine +

                  "while (@dt_aux <> @dt_fin)  " + Environment.NewLine +
                  "begin  " + Environment.NewLine +
                  "	  if    datepart(weekday,@dt_aux) = 1  " + Environment.NewLine +
                  "	  begin  " + Environment.NewLine +
                  "			select @domingos =  @domingos + 1   " + Environment.NewLine +
                  "	  end   " + Environment.NewLine + Environment.NewLine +

                  "	  select @dt_aux = dateadd(day,1,@dt_aux)    " + Environment.NewLine +
                  "end   " + Environment.NewLine +
                  "select @dias =  datediff(dd,@dt_ini,@dt_fin) + 1 - @domingos --+ @habiles - @inhabiles   " + Environment.NewLine + Environment.NewLine +

                  "select @dias " + Environment.NewLine;

            int vigencia = 0;
            object aux;
            if (conexion.AbrirConexion())
            {
                conexion.Instruccion = sql;
                aux = conexion.EjecutarEscalar();
                if (aux != null)
                    vigencia = Convert.ToInt32(aux);
                else
                    vigencia = 10;
            }

            string clientes = string.Empty;

            foreach (DataRow dr in dsrulecondition.Tables[0].Rows)
            {
                if (clientes.Length > 0)
                {
                    clientes += ", ";
                }

                clientes += dr["idcliente"].ToString();
            }

            sql = "	declare @dt_ini smalldatetime, @dt_fin smalldatetime, @dt_aux smalldatetime " + Environment.NewLine +
                "declare @domingos int, @dias int" + Environment.NewLine + Environment.NewLine +

                "select      @dt_fin     =     '03/31/2012' " + Environment.NewLine +
                "select      @dt_ini     =     '03/01/2012' " + Environment.NewLine +
                "select      @dt_aux     =     @dt_ini " + Environment.NewLine +
                "select      @domingos   =     0" + Environment.NewLine + Environment.NewLine +

                "while (@dt_aux <> @dt_fin)" + Environment.NewLine +
                "begin" + Environment.NewLine +
                "	  if    datepart(weekday,@dt_aux) = 1" + Environment.NewLine +
                "	  begin" + Environment.NewLine +
                "			select @domingos =  @domingos + 1 " + Environment.NewLine +
                "	  end" + Environment.NewLine + Environment.NewLine +

                "	  select @dt_aux = dateadd(day,1,@dt_aux)  " + Environment.NewLine +
                "end" + Environment.NewLine + Environment.NewLine +

                "select @dias =  datepart(day,@dt_fin) - @domingos --+ @habiles - @inhabiles" + Environment.NewLine + Environment.NewLine +

                "select codbi, pro, uv=sum(venta),cf = sum(cf), cu = sum(cu), " + Environment.NewLine +
                "   uv_dia = sum(venta*1.0)/@dias, cf_dia = sum(cf)/@dias, uv_dia = sum(cu)/@dias, costo = sum(costop)/sum(cf) " + Environment.NewLine +
                "from factventas fv (nolock) " + Environment.NewLine +
                "inner join dimclientes dc (nolock) on fv.keycliente = dc.keycliente " + Environment.NewLine +
                "inner join dim_tiempos dt (nolock) on fv.keytiempo = dt.keytiempo " + Environment.NewLine +
                "inner join dimregion dr (nolock) on fv.keyregion = dr.keyregion " + Environment.NewLine +
                "where dt.año = 2012 " + Environment.NewLine +
                "  and dt.mes = 4 " + Environment.NewLine +
                "  and dr.puntovta = 31 " + Environment.NewLine +
                "  and dc.codbi in (" + clientes + ") " + Environment.NewLine +
                "group by codbi, pro" + Environment.NewLine +
                "order by codbi, pro " + Environment.NewLine;

            /* Conexio Datawarehouse Cubo Ventas*/
            //conexion = new SQLManejador("Data Source=10.20.129.69;Initial Catalog=ventasEmbe;uid=usr_cuboembe;pwd=cubo2011");
            cadcon = ConfigurationManager.ConnectionStrings["VentasEmbe_Connection"].ConnectionString;
            conexion = new SQLManejador(cadcon);
            if (conexion.AbrirConexion())
            {
                conexion.Instruccion = sql;
                dt = conexion.EjecutarConsulta(new DataTable("ventas"));
            }

            dsrulecondition.Tables.Add(dt);

            dsrulecondition.Tables["resulset"].PrimaryKey = new DataColumn[] { dsrulecondition.Tables["resulset"].Columns["idcliente"] };
            dsrulecondition.Tables["lista"].PrimaryKey = new DataColumn[] { dsrulecondition.Tables["lista"].Columns["codope"] };
            dsrulecondition.Tables["ventas"].PrimaryKey = new DataColumn[] { dsrulecondition.Tables["ventas"].Columns["codbi"], dsrulecondition.Tables["ventas"].Columns["pro"] };

            DataRelation drellistsvtas = new DataRelation("listavtas", dsrulecondition.Tables["lista"].Columns["codope"], dsrulecondition.Tables["ventas"].Columns["pro"], false);
            DataRelation drelvtaslista = new DataRelation("vtaslista", dsrulecondition.Tables["ventas"].Columns["pro"], dsrulecondition.Tables["lista"].Columns["codope"], false);
            DataRelation drelresulsetvtas = new DataRelation("rsvtas", dsrulecondition.Tables["resulset"].Columns["idcliente"], dsrulecondition.Tables["ventas"].Columns["codbi"], false);


            dsrulecondition.Relations.Add(drellistsvtas);
            dsrulecondition.Relations.Add(drelvtaslista);
            dsrulecondition.Relations.Add(drelresulsetvtas);

            DataRow[] childRows;
            DataRow[] grandchildRows;

            double ingreso = 0;
            double ingresodesc = 0;
            double vta_dia = 0.0;
            double vta_total = 0.0;
            double precio = 0.0;
            double preciodesc = 0.0;
            double costo = 0.0;
            double ubruta = 0.0;
            double ubrutadesc = 0.0;
            double desc = 0;

            double globalingreso = 0;
            double globalingresodesc = 0;
            double globalvta_dia = 0.0;
            double globalvta_total = 0.0;
            double globalprecio = 0.0;
            double globalpreciodesc = 0.0;
            double globalcosto = 0.0;
            double globalubruta = 0.0;
            double globalubrutadesc = 0.0;
            double globaldesc = 0;

            double? compcte = compromiso;
            double? compglobal = compromiso;

            DataSet dsFinal = new DataSet();
            DataTable dtGlobal = null;
            DataTable dtCtes = null;

            sql = "select Tipo = '                   ', Crec = 0.0, Vta_dia = 0.0, Fisicas = 0.0, Precio = 0.0, Ingreso = 0.0, Ubruta = 0.0, dif=0.0, difbruta = 0.0";
            if (conexion.AbrirConexion())
            {
                conexion.Instruccion = sql;
                dtGlobal = conexion.EjecutarConsulta(new DataTable("dtGlobal"));
            }

            sql = "select IdCliente= 0, Tipo = '                   ', Crec = 0.0, Vta_dia = 0.0, Fisicas = 0.0, Precio = 0.0, Ingreso = 0.0, Ubruta = 0.0, dif=0.0, difbruta = 0.0";
            if (conexion.AbrirConexion())
            {
                conexion.Instruccion = sql;
                dtCtes = conexion.EjecutarConsulta(new DataTable("dtCtes"));
            }

            foreach (DataRow drpadre in dsrulecondition.Tables["resulset"].Rows)
            {
                ingreso = 0;
                ingresodesc = 0;
                vta_dia = 0.0;
                vta_total = 0.0;
                precio = 0.0;
                preciodesc = 0.0;
                costo = 0.0;
                ubruta = 0.0;
                ubrutadesc = 0.0;

                childRows = drpadre.GetChildRows(dsrulecondition.Relations["rsvtas"]);
                if (childRows.Length > 0)
                {
                    foreach (DataRow drhijo in childRows)
                    {
                        grandchildRows = drhijo.GetChildRows(dsrulecondition.Relations["vtaslista"]);
                        if (grandchildRows.Length > 0)
                        {
                            foreach (DataRow drnieto in grandchildRows)
                            {
                                //combiar el 10 por los dias habiles(sin domingos) de la vigencia de la lista de descuento
                                vta_dia += Convert.ToDouble(drhijo["cf_dia"]);
                                vta_total += (Convert.ToDouble(drhijo["cf_dia"]) * vigencia);
                                precio += Convert.ToDouble(drnieto["precio"]);

                                ingreso += (Convert.ToDouble(drhijo["cf_dia"]) * vigencia) * Convert.ToDouble(drnieto["precio"]);

                                desc = 0;
                                if (Convert.ToDouble(drnieto["ope_pesos"]) > 0)
                                {
                                    desc = Convert.ToDouble(drnieto["ope_pesos"]);
                                }
                                else if (Convert.ToDouble(drnieto["ope_porcentaje"]) > 0)
                                {
                                    desc = Convert.ToDouble(drnieto["precio"]) * (Convert.ToDouble(drnieto["ope_porcentaje"]) / 100);
                                }

                                ingresodesc += (Convert.ToDouble(drhijo["cf_dia"]) * vigencia) * (Convert.ToDouble(drnieto["precio"]) - desc);
                                preciodesc += Convert.ToDouble(drnieto["precio"]) - desc;

                                costo += ((Convert.ToDouble(drhijo["cf_dia"]) * vigencia) * Convert.ToDouble(drhijo["costo"]));
                                ubruta += (Convert.ToDouble(drhijo["cf_dia"]) * vigencia) * Convert.ToDouble(drnieto["precio"]) - ((Convert.ToDouble(drhijo["cf_dia"]) * vigencia) * Convert.ToDouble(drhijo["costo"]));
                                ubrutadesc += (Convert.ToDouble(drhijo["cf_dia"]) * vigencia) * (Convert.ToDouble(drnieto["precio"]) - desc) - ((Convert.ToDouble(drhijo["cf_dia"]) * vigencia) * Convert.ToDouble(drhijo["costo"]));

                            }
                        }
                    }

                    if (ingreso > 0.0)
                    {
                        //por cliente
                        DataRow drnew = dtCtes.NewRow();

                        if (dtCtes.Rows.Count == 1)
                            dtCtes.Rows.RemoveAt(0);

                        drnew["IdCliente"] = Convert.ToInt32(drpadre["idcliente"]);
                        drnew["Ingreso"] = Math.Round(ingreso, 2);
                        drnew["Vta_dia"] = Math.Round(vta_dia, 2);
                        drnew["Fisicas"] = Math.Round(vta_total, 2);
                        drnew["Precio"] = Math.Round(Math.Round(ingreso, 2) / Math.Round(vta_total, 2), 2);
                        drnew["Ubruta"] = Math.Round(ubruta, 2);
                        drnew["Tipo"] = "Sin Descuento";
                        dtCtes.Rows.Add(drnew);

                        drnew = dtCtes.NewRow();
                        drnew["IdCliente"] = Convert.ToInt32(drpadre["idcliente"]);
                        drnew["Ingreso"] = Math.Round(ingresodesc, 2);
                        drnew["Precio"] = Math.Round(Math.Round(ingresodesc, 2) / Math.Round(vta_total, 2), 2);
                        drnew["Ubruta"] = Math.Round(ubrutadesc, 2);
                        drnew["Tipo"] = "Con Descuento";
                        dtCtes.Rows.Add(drnew);

                        drnew = dtCtes.NewRow();
                        double crecimiento = Math.Round(100 - ((ingresodesc * 100) / ingreso), 2);
                        drnew["IdCliente"] = Convert.ToInt32(drpadre["idcliente"]);
                        drnew["Crec"] = crecimiento;
                        drnew["Vta_dia"] = Math.Round(vta_dia * (1 + (crecimiento / 100)), 2);
                        drnew["Fisicas"] = Math.Round(vta_total * (1 + (crecimiento / 100)), 2);
                        drnew["Ingreso"] = Math.Round(ingreso, 2);
                        drnew["Ubruta"] = Math.Round(ubruta, 2);
                        drnew["Tipo"] = "Punto Equilibrio";
                        dtCtes.Rows.Add(drnew);

                        drnew = dtCtes.NewRow();
                        if (compromiso == null)
                            compcte = crecimiento;
                        drnew["IdCliente"] = Convert.ToInt32(drpadre["idcliente"]);
                        drnew["Crec"] = compcte;
                        drnew["Vta_dia"] = Math.Round(Convert.ToDouble(vta_dia * (1 + (compcte / 100))), 2);
                        drnew["Fisicas"] = Math.Round(Convert.ToDouble(vta_total * (1 + (compcte / 100))), 2);
                        drnew["Ingreso"] = Math.Round(Convert.ToDouble(ingreso * (1 + (compcte / 100))), 2);
                        //drnew["ubruta"] = (ingreso * (1 + (Convert.ToDouble(compromiso) / 100))) - costo;
                        drnew["Tipo"] = "Compromiso";
                        dtCtes.Rows.Add(drnew);

                        //Global
                        globalingreso += ingreso;
                        globalingresodesc += ingresodesc;
                        globalvta_dia += vta_dia;
                        globalvta_total += vta_total;
                        globalprecio += precio;
                        globalpreciodesc += preciodesc;
                        globalcosto += costo;
                        globalubruta += ubruta;
                        globalubrutadesc += ubrutadesc;
                        globaldesc += desc;
                    }
                }
            }

            //por cliente
            DataRow drglobal = dtGlobal.NewRow();

            if (dtGlobal.Rows.Count == 1)
                dtGlobal.Rows.RemoveAt(0);
            
            drglobal["Ingreso"] = Math.Round(globalingreso, 2);
            drglobal["Vta_dia"] = Math.Round(globalvta_dia, 2);
            drglobal["Fisicas"] = Math.Round(globalvta_total, 2);
            drglobal["Precio"] = globalprecio;
            drglobal["Ubruta"] = Math.Round(globalubruta, 2);
            drglobal["Tipo"] = "Sin Descuento";
            dtGlobal.Rows.Add(drglobal);

            drglobal = dtGlobal.NewRow();
            drglobal["Ingreso"] = Math.Round(globalingresodesc, 2);
            drglobal["Precio"] = globalpreciodesc;
            drglobal["Ubruta"] = Math.Round(globalubrutadesc, 2);
            drglobal["Tipo"] = "Con Descuento";
            dtGlobal.Rows.Add(drglobal);

            drglobal = dtGlobal.NewRow();
            double crecglobal = Math.Round(100 - ((globalingresodesc * 100) / globalingreso), 2);
            if (double.IsNaN(crecglobal))
            {
                crecglobal = 0;
            }
            drglobal["Crec"] = crecglobal;
            drglobal["Vta_dia"] = Math.Round(globalvta_dia * (1 + (crecglobal / 100)), 2);
            drglobal["Fisicas"] = Math.Round(globalvta_total * (1 + (crecglobal / 100)), 2);
            drglobal["Ingreso"] = Math.Round(globalingreso, 2);
            drglobal["Ubruta"] = Math.Round(globalubruta, 2);
            drglobal["Tipo"] = "Punto Equilibrio";
            dtGlobal.Rows.Add(drglobal);

            drglobal = dtGlobal.NewRow();
            if (compromiso == null)
                compglobal = crecglobal;
            drglobal["Crec"] = compglobal;
            drglobal["Vta_dia"] = Math.Round(Convert.ToDouble(globalvta_dia * (1 + (compglobal / 100))), 2);
            drglobal["Fisicas"] = Math.Round(Convert.ToDouble(globalvta_total * (1 + (compglobal / 100))), 2);
            drglobal["Ingreso"] = Math.Round(Convert.ToDouble(globalingreso * (1 + (compglobal / 100))), 2);
            //drglobal["ubruta"] = (ingreso * (1 + (Convert.ToDouble(compromiso) / 100))) - costo;
            drglobal["Tipo"] = "Compromiso";
            dtGlobal.Rows.Add(drglobal);

            #region "Version Anterior"

            //foreach (DataRelation dr in dsrulecondition.Relations)
            //{
            //    foreach (DataRow drc in dsrulecondition.Tables["resulset"].Rows)
            //    {
            //        foreach (DataRow drpadre in dsrulecondition.Tables["lista"].Rows)
            //        {
            //            childRows = drpadre.GetChildRows(dr);
            //            foreach (DataRow drhijo in childRows)
            //            {
            //                ingreso += (Convert.ToDouble(drhijo["cf_dia"]) * 10) * Convert.ToDouble(drpadre["precio"]);

            //                desc = 0;
            //                if (Convert.ToDouble(drpadre["ope_pesos"]) > 0)
            //                {
            //                    desc = Convert.ToDouble(drpadre["ope_pesos"]);
            //                }
            //                else if (Convert.ToDouble(drpadre["ope_porcentaje"]) > 0)
            //                {
            //                    desc = Convert.ToDouble(drpadre["precio"]) * (Convert.ToDouble(drpadre["ope_porcentaje"]) / 100);
            //                }

            //                ingresodesc += (Convert.ToDouble(drhijo["cf_dia"]) * 10) * (Convert.ToDouble(drpadre["precio"]) - desc);

            //                vta_dia += Convert.ToDouble(drhijo["cf_dia"]);
            //                vta_total += (Convert.ToDouble(drhijo["cf_dia"]) * 10);
            //                precio += Convert.ToDouble(drpadre["precio"]);
            //                preciodesc += Convert.ToDouble(drpadre["precio"]) - desc;

            //                costo += ((Convert.ToDouble(drhijo["cf_dia"]) * 10) * Convert.ToDouble(drhijo["costo"]));
            //                ubruta += ingreso - ((Convert.ToDouble(drhijo["cf_dia"]) * 10) * Convert.ToDouble(drhijo["costo"]));
            //                ubrutadesc += ingresodesc - ((Convert.ToDouble(drhijo["cf_dia"]) * 10) * Convert.ToDouble(drhijo["costo"]));

            //            }
            //        }
            //    }

            //      DataRow drnew = datos.NewRow();
            //      drnew["ingreso"] = ingreso;
            //      drnew["vta_dia"] = vta_dia;
            //      drnew["fisicas"] = vta_total;
            //      drnew["precio"] = precio;
            //      drnew["ubruta"] = ubruta;
            //      drnew["tipo"] = "normal";
            //      datos.Rows.Add(drnew);

            //      drnew = datos.NewRow();
            //      drnew["ingreso"] = ingresodesc;
            //      drnew["precio"] = preciodesc;
            //      drnew["ubruta"] = ubrutadesc;
            //      drnew["tipo"] = "descuento";
            //      datos.Rows.Add(drnew);

            //      drnew = datos.NewRow();
            //      double crecimiento = 100 - ((ingresodesc * 100) / ingreso);
            //      drnew["crec"] = crecimiento;
            //      drnew["vta_dia"] = vta_dia * (1 + (crecimiento / 100));
            //      drnew["fisicas"] = vta_total * (1 + (crecimiento / 100));
            //      drnew["ingreso"] = ingreso;
            //      drnew["ubruta"] = ubruta;
            //      drnew["tipo"] = "pe";
            //      datos.Rows.Add(drnew);

            //      drnew = datos.NewRow();
            //      if (compromiso == null)
            //          compromiso = crecimiento;

            //      drnew["crec"] = compromiso;
            //      drnew["vta_dia"] = vta_dia * (1 + (Convert.ToDouble(compromiso)/ 100));
            //      drnew["fisicas"] = vta_total * (1 + (Convert.ToDouble(compromiso)/ 100));
            //      drnew["ingreso"] = ingreso * (1 + (Convert.ToDouble(compromiso) / 100));
            //      drnew["ubruta"] = (ingreso * (1 + (Convert.ToDouble(compromiso) / 100))) - costo;
            //      drnew["tipo"] = "compromiso";
            //      datos.Rows.Add(drnew);

            //      datos.Rows.RemoveAt(0);


            //}

            #endregion

            dsFinal.Tables.Add(dtGlobal);
            dsFinal.Tables.Add(dtCtes);

            return dsFinal;
        }



    }
}