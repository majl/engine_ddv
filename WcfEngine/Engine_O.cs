﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Xml;
using System.Collections;
using System.Xml.Serialization;
using System.Configuration;

namespace WcfEngine
{
    public class Engine
    {
        public Engine()
        {

        }

        public DataSet ExecuteInsert(XmlDocument xDoc)
        {
            Hashtable operadores = new Hashtable();
            operadores.Add("eq", " = ");
            operadores.Add("in", " in (");
            operadores.Add("may", " > ");
            operadores.Add("men", " < ");

            entidad estr = new entidad();
            rulecondition rcstr = new rulecondition();

            DataSet dsResult = new DataSet();

            DataSet dsrulecondition = new DataSet();
            dsrulecondition.Tables.Add("resulset");

            XmlNodeReader reader = new XmlNodeReader(xDoc.DocumentElement);
            XmlSerializer ser = new XmlSerializer(estr.GetType());

            object obj = ser.Deserialize(reader);

            entidad entidad = (entidad)obj;

            #region Objetos

            linkentity le = null;

            #endregion

            #region Variables

            string sql = string.Empty;
            string sql_in = string.Empty;
            string sql_in2 = string.Empty;
            string sql_in3 = string.Empty;

            string sql_condition = string.Empty;

            string sql_le = string.Empty;
            string sql_le2 = string.Empty;

            string sql_where = string.Empty;
            string sql_where2 = string.Empty;
            string sql_complemento = string.Empty;
            string cadcon = string.Empty;
            #endregion

            bool op_nulo = false;

            if (entidad.rule.Count() > 0)
            {
                sql = String.Format("SELECT  catruta.IdRuta, {0}.{1}, oportunidadid ='{2}', noportunidad = '{3}',\n", entidad.name, entidad.retrieveattribute, entidad.taskid, entidad.task);
                sql += String.Format("       media = '{0}', ddv = '{1}', \n       fechaini = '{2}', fechafin = '{3}', genera = {4} \n", entidad.media, entidad.ddv, entidad.fechaini, entidad.fechafin, entidad.genera);
                sql += "  FROM " + entidad.name + System.Environment.NewLine;

                sql += "inner join rutacliente catrutacliente on cliente.idcliente = catrutacliente.idcliente" + System.Environment.NewLine;
                sql += "inner join ruta catruta on catrutacliente.idruta = catruta.idruta" + System.Environment.NewLine;

                sql = sql.Replace("{", "").Replace("}", "");

                foreach (rule regla in entidad.rule)
                {
                    sql_where = string.Empty;

                    if (regla.rulecondition.Count() > 0)
                    {
                        foreach (rulecondition rulecondition in regla.rulecondition)
                        {
                            sql_le = string.Empty;
                            sql_condition = string.Empty;
                            sql_in2 = string.Empty;

                            foreach (condition condicion in rulecondition.condition)
                            {
                                sql_in2 = string.Empty;

                                if (!condicion.operatortype.Equals("null"))
                                {
                                    if (!String.IsNullOrEmpty(sql_condition))
                                        sql_condition += " and ";

                                    if (condicion.valuelist.Count() == 0)
                                    {
                                        sql_condition += entidad.name + "." + condicion.attribute + operadores[condicion.operatortype].ToString() + condicion.value;
                                    }
                                    else
                                    {
                                        sql_condition += entidad.name + "." + condicion.attribute + operadores[condicion.operatortype].ToString();
                                        foreach (string value in condicion.valuelist)
                                        {
                                            sql_in2 += String.IsNullOrEmpty(sql_in2) ? value : "," + value;
                                        }
                                        sql_in2 += ")";

                                        sql_condition += sql_in2;
                                    }
                                }
                                else
                                    op_nulo = true;
                            }

                            if (rulecondition.linkentity != null)
                            {
                                le = rulecondition.linkentity;
                                op_nulo = false;
                                sql_le = "select * from " + le.name;
                                sql_le2 = string.Empty;
                                sql_in3 = string.Empty;

                                #region region link de link
                                foreach (linkentity le1 in le.linkentitie)
                                {
                                    sql_le += " inner join ";
                                    for (int i = 0; i < le1.from.Count(); i++)
                                    {
                                        sql_le += (i > 0 ? " and " : le1.name + " on ");

                                        sql_le += le.name + "." + le1.from.ElementAt(i);
                                        sql_le += " = " + le1.name + "." + le1.to.ElementAt(i);
                                    }

                                    foreach (condition condicion in le1.condition)
                                    {
                                        sql_in3 = string.Empty;

                                        if (!condicion.operatortype.Equals("null"))
                                        {
                                            if (condicion.valuelist.Count() == 0)
                                            {
                                                sql_le2 += (string.IsNullOrEmpty(sql_le2)) ? " Where " : "   and ";
                                                sql_le2 += le1.name + "." + condicion.attribute + operadores[condicion.operatortype].ToString() + condicion.value;
                                            }
                                            else
                                            {
                                                if (!String.IsNullOrEmpty(sql_le2))
                                                    sql_le2 += " and ";
                                                else
                                                    sql_le2 += " Where ";

                                                sql_le2 += le1.name + "." + condicion.attribute + operadores[condicion.operatortype].ToString();
                                                foreach (string value in condicion.valuelist)
                                                {
                                                    sql_in3 += String.IsNullOrEmpty(sql_in3) ? value : "," + value;
                                                }
                                                sql_in3 += ")";

                                                sql_le2 += sql_in3;
                                            }
                                        }
                                        else
                                            op_nulo = true;
                                    }
                                }

                                #endregion

                                sql_le += (string.IsNullOrEmpty(sql_le2)) ? " Where " : sql_le2 + " and ";
                                sql_in = string.Empty;

                                for (int i = 0; i < le.from.Count(); i++)
                                {
                                    sql_le += (i > 0 ? " and " : "");

                                    sql_le += entidad.name + "." + le.from.ElementAt(i);
                                    sql_le += " = " + le.name + "." + le.to.ElementAt(i);
                                }

                                foreach (condition condicion in le.condition)
                                {
                                    sql_in = string.Empty;

                                    if (!condicion.operatortype.Equals("null"))
                                    {
                                        if (condicion.valuelist.Count() == 0)
                                            sql_le += " and " + le.name + "." + condicion.attribute + operadores[condicion.operatortype].ToString() + condicion.value;
                                        else
                                        {
                                            if (!String.IsNullOrEmpty(sql_le))
                                                sql_le += " and ";

                                            sql_le += le.name + "." + condicion.attribute + operadores[condicion.operatortype].ToString();
                                            foreach (string value in condicion.valuelist)
                                            {
                                                sql_in += String.IsNullOrEmpty(sql_in) ? value : "," + value;
                                            }
                                            sql_in += ")";

                                            sql_le += sql_in;
                                        }
                                    }
                                    else
                                        op_nulo = true;
                                }
                            }


                            if (string.IsNullOrEmpty(sql_le))
                            {
                                if (!string.IsNullOrEmpty(sql_condition)) sql_condition += System.Environment.NewLine;
                                sql_where += (string.IsNullOrEmpty(sql_where)) ? " WHERE " + sql_condition : "   AND " + sql_condition;
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(sql_condition)) sql_condition += System.Environment.NewLine + "   AND ";
                                sql_where += (string.IsNullOrEmpty(sql_where)) ? " WHERE " + sql_condition : "   AND " + sql_condition;
                                sql_where += (op_nulo ? "not " : "") + "exists (" + sql_le + ")" + System.Environment.NewLine;
                            }
                        }

                        //sql_complemento = "ORDER BY catcedis.ncedis, catruta.nruta, Cliente.idcliente" + System.Environment.NewLine;

                        
                        //cadcon = "Data Source=10.2.129.131;Initial Catalog=DWHSTD;uid=sa;pwd=desarrollo";
                        //cadcon = "Data Source=200.57.131.32;Initial Catalog=DWHSTD;uid=usr_operule;pwd=-S3rvic3-";
                        cadcon = ConfigurationManager.ConnectionStrings["DWHSTD_Connection"].ConnectionString;
                        SQLManejador conexion = new SQLManejador(cadcon);
                        if (conexion.AbrirConexion())
                        {
                            conexion.Instruccion = sql + sql_where + sql_complemento;
                            dsrulecondition.Merge(conexion.EjecutarConsulta(dsrulecondition.Tables["resulset"]));
                        }
                    }
                }
            }


            DataView Dv = dsrulecondition.Tables["resulset"].DefaultView;
            dsResult.Tables.Add(Dv.ToTable(true, "idruta", "idcliente", "oportunidadid", "noportunidad", "media", "ddv", "fechaini", "fechafin","genera"));

            return dsResult;
        }


        public DataSet ExecutePreview(XmlDocument xDoc)
        {
            Hashtable operadores = new Hashtable();
            operadores.Add("eq", " = ");
            operadores.Add("in", " in (");
            operadores.Add("may", " > ");
            operadores.Add("men", " < ");

            entidad estr = new entidad();
            rulecondition rcstr = new rulecondition();

            DataSet dsResult = new DataSet();

            DataSet dsrulecondition = new DataSet();
            dsrulecondition.Tables.Add("resulset");

            XmlNodeReader reader = new XmlNodeReader(xDoc.DocumentElement);
            XmlSerializer ser = new XmlSerializer(estr.GetType());

            object obj = ser.Deserialize(reader);

            entidad entidad = (entidad)obj;

            #region Objetos

            linkentity le = null;

            #endregion

            #region Variables

            string sql = string.Empty;
            string sql_in = string.Empty;
            string sql_in2 = string.Empty;
            string sql_in3 = string.Empty;

            string sql_condition = string.Empty;

            string sql_le = string.Empty;
            string sql_le2 = string.Empty;

            string sql_where = string.Empty;
            string sql_where2 = string.Empty;
            string sql_complemento = string.Empty;
            string cadcon = string.Empty;
            #endregion

            bool op_nulo = false;

            if (entidad.rule.Count() > 0)
            {
                sql = "SELECT DISTINCT catcedis.ncedis, catruta.ruta, catruta.nruta, Cliente.idcliente, Cliente.ncliente, " + System.Environment.NewLine;
                sql += "       Cliente.direccion, catgiro.ngiro, cattamanio.ntamanio, " + System.Environment.NewLine;
                sql += "       catpotencial.npotencial, Cliente.cajas, Cliente.skus" + System.Environment.NewLine;

                sql += "  FROM " + entidad.name + System.Environment.NewLine;

                sql += "inner join giro catgiro on cliente.idgiro = catgiro.idgiro" + System.Environment.NewLine;
                sql += "inner join tamanio cattamanio on cliente.idtamanio = cattamanio.idtamanio" + System.Environment.NewLine;
                sql += "inner join potencial catpotencial on cliente.idpotencial = catpotencial.idpotencial" + System.Environment.NewLine;
                sql += "inner join rutacliente catrutacliente on cliente.idcliente = catrutacliente.idcliente" + System.Environment.NewLine;
                sql += "inner join ruta catruta on catrutacliente.idruta = catruta.idruta" + System.Environment.NewLine;
                sql += "inner join cedis catcedis on catruta.idcedis = catcedis.idcedis" + System.Environment.NewLine;
                foreach (rule regla in entidad.rule)
                {
                    sql_where = string.Empty;


                    if (regla.rulecondition.Count() > 0)
                    {
                        foreach (rulecondition rulecondition in regla.rulecondition)
                        {
                            sql_le = string.Empty;
                            sql_condition = string.Empty;
                            sql_in2 = string.Empty;

                            foreach (condition condicion in rulecondition.condition)
                            {
                                sql_in2 = string.Empty;

                                if (!condicion.operatortype.Equals("null"))
                                {
                                    if (!String.IsNullOrEmpty(sql_condition))
                                        sql_condition += " and ";

                                    if (condicion.valuelist.Count() == 0)
                                    {
                                        sql_condition += entidad.name + "." + condicion.attribute + operadores[condicion.operatortype].ToString() + condicion.value;
                                    }
                                    else
                                    {
                                        sql_condition += entidad.name + "." + condicion.attribute + operadores[condicion.operatortype].ToString();
                                        foreach (string value in condicion.valuelist)
                                        {
                                            sql_in2 += String.IsNullOrEmpty(sql_in2) ? value : "," + value;
                                        }
                                        sql_in2 += ")";

                                        sql_condition += sql_in2;
                                    }
                                }
                                else
                                    op_nulo = true;
                            }

                            if (rulecondition.linkentity != null)
                            {
                                le = rulecondition.linkentity;
                                op_nulo = false;
                                sql_le = "select * from " + le.name;
                                sql_le2 = string.Empty;
                                sql_in3 = string.Empty;

                                #region region link de link
                                foreach (linkentity le1 in le.linkentitie)
                                {
                                    sql_le += " inner join ";
                                    for (int i = 0; i < le1.from.Count(); i++)
                                    {
                                        sql_le += (i > 0 ? " and " : le1.name + " on ");

                                        sql_le += le.name + "." + le1.from.ElementAt(i);
                                        sql_le += " = " + le1.name + "." + le1.to.ElementAt(i);
                                    }

                                    foreach (condition condicion in le1.condition)
                                    {
                                        sql_in3 = string.Empty;

                                        if (!condicion.operatortype.Equals("null"))
                                        {
                                            if (condicion.valuelist.Count() == 0)
                                            {
                                                sql_le2 += (string.IsNullOrEmpty(sql_le2)) ? " Where " : "   and ";
                                                sql_le2 += le1.name + "." + condicion.attribute + operadores[condicion.operatortype].ToString() + condicion.value;
                                            }
                                            else
                                            {
                                                if (!String.IsNullOrEmpty(sql_le2))
                                                    sql_le2 += " and ";
                                                else
                                                    sql_le2 += " Where ";

                                                sql_le2 += le1.name + "." + condicion.attribute + operadores[condicion.operatortype].ToString();
                                                foreach (string value in condicion.valuelist)
                                                {
                                                    sql_in3 += String.IsNullOrEmpty(sql_in3) ? value : "," + value;
                                                }
                                                sql_in3 += ")";

                                                sql_le2 += sql_in3;
                                            }
                                        }
                                        else
                                            op_nulo = true;
                                    }
                                }


                                #endregion

                                sql_le += (string.IsNullOrEmpty(sql_le2)) ? " Where " : sql_le2 + " and ";
                                sql_in = string.Empty;

                                for (int i = 0; i < le.from.Count(); i++)
                                {
                                    sql_le += (i > 0 ? " and " : "");

                                    sql_le += entidad.name + "." + le.from.ElementAt(i);
                                    sql_le += " = " + le.name + "." + le.to.ElementAt(i);
                                }

                                foreach (condition condicion in le.condition)
                                {
                                    sql_in = string.Empty;

                                    if (!condicion.operatortype.Equals("null"))
                                    {
                                        if (condicion.valuelist.Count() == 0)
                                            sql_le += " and " + le.name + "." + condicion.attribute + operadores[condicion.operatortype].ToString() + condicion.value;
                                        else
                                        {
                                            if (!String.IsNullOrEmpty(sql_le))
                                                sql_le += " and ";

                                            sql_le += le.name + "." + condicion.attribute + operadores[condicion.operatortype].ToString();
                                            foreach (string value in condicion.valuelist)
                                            {
                                                sql_in += String.IsNullOrEmpty(sql_in) ? value : "," + value;
                                            }
                                            sql_in += ")";

                                            sql_le += sql_in;
                                        }
                                    }
                                    else
                                        op_nulo = true;
                                }
                            }


                            if (string.IsNullOrEmpty(sql_le))
                            {
                                if (!string.IsNullOrEmpty(sql_condition)) sql_condition += System.Environment.NewLine;
                                sql_where += (string.IsNullOrEmpty(sql_where)) ? " WHERE " + sql_condition : "   AND " + sql_condition;
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(sql_condition)) sql_condition += System.Environment.NewLine + "   AND ";
                                sql_where += (string.IsNullOrEmpty(sql_where)) ? " WHERE " + sql_condition : "   AND " + sql_condition;
                                sql_where += (op_nulo ? "not " : "") + "exists (" + sql_le + ")" + System.Environment.NewLine;
                            }
                        }

                        //sql_complemento = " AND idruta = 910" + System.Environment.NewLine;
                        //sql_complemento = " AND idruta = 1" + System.Environment.NewLine;
                        sql_complemento = "ORDER BY catcedis.ncedis, catruta.ruta,catruta.nruta, Cliente.idcliente" + System.Environment.NewLine;
                        cadcon = ConfigurationManager.ConnectionStrings["DWHSTD_Connection"].ConnectionString;
                        SQLManejador conexion = new SQLManejador(cadcon);
                        if (conexion.AbrirConexion())
                        {
                            Console.WriteLine(sql + sql_where + sql_complemento);
                            conexion.Instruccion = sql + sql_where + sql_complemento;
                            dsrulecondition.Merge(conexion.EjecutarConsulta(dsrulecondition.Tables["resulset"]));
                        }
                    }
                }
            }


            DataView Dv = dsrulecondition.Tables["resulset"].DefaultView;
            dsResult.Tables.Add(Dv.ToTable(true, "ruta", "nruta", "idcliente", "ncliente", "direccion", "ngiro", "ntamanio", "npotencial", "cajas", "skus"));
            
            return dsResult;
        }
    }
}