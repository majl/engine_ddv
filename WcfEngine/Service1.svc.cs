﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Xml;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.EntityDataReader;
using System.Drawing;
using System.Data.Objects;
using System.Globalization;
using System.ServiceModel.Activation;
using System.IO;
using System.Xml.Serialization;
using WcfEngine.Utils;
using Dapper;

namespace WcfEngine
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
     public class Service1 : IService1
     {
        public IList<ClientesPreview> GetPreview(string xmlBase64)
        {

           
            byte[] encodedDataAsBytes = System.Convert.FromBase64String(xmlBase64);
            string returnValue = System.Text.ASCIIEncoding.ASCII.GetString(encodedDataAsBytes);

            XmlDocument xDoc = new XmlDocument();
            xDoc.LoadXml(returnValue);
            
            Engine motor = new Engine();
            DataSet ds = motor.ExecutePreview(xDoc);

            List<ClientesPreview> c = new List<ClientesPreview>();
            if (ds != null)
            {
                c = ds.Tables[0].AsEnumerable().Select(dr => new ClientesPreview(){
                    nomjefe = dr.Field<String>(0),
                    nomsupervisor = dr.Field<String>(1),
                    NCategoria = dr.Field<String>(2),
                    idruta= dr.Field<int>(3).ToString(),
                    nruta = dr.Field<String>(4),
                    idcliente = dr.Field<int>(5).ToString(), 
                    ncliente = dr.Field<String>(6),
                    direccion = dr.Field<String>(7),
                    ngiro = dr.Field<String>(8),
                    ntamanio = dr.Field<String>(9),
                    npotencial = dr.Field<String>(10),
                    cajas = dr.Field<int>(11).ToString(),
                    skus = dr.Field<int>(12).ToString(),
                    ncanalrtm = dr.Field<String>(13),
                    ncedis = dr.Field<String>(14),
                    AccountId = dr.Field<String>(15)
                }).ToList();
            }
         
            return c;            
        }
         
        public string SaveData(string xmlBase64, string fechaRegistro)
        {
           // fechaRegistro = DateTime.Now.ToString();
            try
            {
                CultureInfo cultureProvider = new CultureInfo("es-MX");
                DateTime FechaRegistro = DateTime.Now;
                
                DayOfWeek dia = FechaRegistro.DayOfWeek;

                byte[] encodedDataAsBytes = Convert.FromBase64String(xmlBase64);
                string returnValue = ASCIIEncoding.ASCII.GetString(encodedDataAsBytes);
                
                XmlDocument xDoc = new XmlDocument();
                xDoc.LoadXml(returnValue);
               
                Engine motor = new Engine();
                DataSet ds = motor.ExecuteInsert(xDoc);
                DataTable table = ds.Tables[0];
                EnumerableRowCollection<DataRow> TableOportunidades = table.AsEnumerable().OrderBy(o => o.Field <int> ("idruta"));                
                int c = 0;
                int IdRutaAux = 0;
                List<OportunidadesCliente> oportunidadescliente = new List<OportunidadesCliente>();
               // var yu = TableOportunidades.Select(p=>p[Opo])

                List<OportunidadesCliente> oportunidadesClienteToUpate = new List<OportunidadesCliente>();

                using (DWHSTDEntities entities = new DWHSTDEntities())
                {
                    if (TableOportunidades != null && TableOportunidades.Count() > 0)
                    {
                        DataRow row = TableOportunidades.First();
                        if (row != null)
                        {
                            Guid IdOportunidad = Guid.Parse(row["oportunidadid"].ToString());
                            var oportunidadResult = entities.Oportunidades.FirstOrDefault(op => op.IdOportunidad == IdOportunidad);
                            if (oportunidadResult == null)
                            {
                                //Not exists
                                Oportunidades oportunity = new Oportunidades();
                                oportunity = fillOportunity(oportunity, row);
                                oportunity.IdOportunidad = IdOportunidad;
                                oportunity.xml = xmlBase64;
                                entities.Oportunidades.AddObject(oportunity);
                            }
                            else
                            {
                                //Exists
                                oportunidadResult = fillOportunity(oportunidadResult, row);
                            }
                            entities.SaveChanges();
                            SaveImages(IdOportunidad);
                        
                            var currentOportunitiesClient = from o in entities.OportunidadesCliente
                                                            where o.IdOportunidad == IdOportunidad
                                                            select o;
                            if(currentOportunitiesClient.Any())
                            {
                                List<OportunidadesCliente> insert = new List<OportunidadesCliente>();
                                List<OportunidadesCliente> update = new List<OportunidadesCliente>();

                                oportunidadescliente = convertToList(TableOportunidades);
                                oportunidadescliente.ForEach(opc => {
                                    OportunidadesCliente result = currentOportunitiesClient
                                        .FirstOrDefault(cOpc => cOpc.IdOportunidad == opc.IdOportunidad 
                                            && cOpc.IdCliente == opc.IdCliente && cOpc.IdRuta == opc.IdRuta
                                            && cOpc.IdCategoria == opc.IdCategoria);
                                    if (result != null)
                                    {
                                        opc.IdOportunidadCliente = result.IdOportunidadCliente;
                                        if (result.Estatus.Value.Equals(1))
                                        {
                                            opc.Estatus = 0;
                                        }
                                        update.Add(opc);
                                    }
                                    else
                                    {
                                        insert.Add(opc);
                                    }
                                });
                                mayBulkInsert(insert);
                                mayBulkUpdate(update);
                            }
                            else
                            {
                                oportunidadescliente = convertToList(TableOportunidades);
                                mayBulkInsert(oportunidadescliente);
                            }
                        }   
                    }
                    #region OldMethod
                //    foreach (DataRow row in TableOportunidades)
                //    {
                //        Guid IdOportunidad = Guid.Parse(row["oportunidadid"].ToString());
                //        int IdCliente = int.Parse(row["idcliente"].ToString());
                //        int IdRuta = int.Parse(row["idruta"].ToString());
                //        int genera = int.Parse(row["genera"].ToString());

                //        var opetype = false; //TEMPORAL dsOpeType.Tables["resulset"].Rows[0]["ope_type"];
                //        if (c == 0)
                //        {                            
                //            //busca la oportunidad para ver si existe
                //            var query = from o in entities.Oportunidades
                //                        where o.IdOportunidad == IdOportunidad
                //                        select o;
                //            if (query.Count() == 0)
                //            {
                //                Oportunidades oportunidad = new Oportunidades();
                //                oportunidad.IdOportunidad = IdOportunidad;
                //                oportunidad.NOportunidad = row["noportunidad"].ToString();
                //                oportunidad.Media = row["media"].ToString();
                //                oportunidad.Ddv = row["ddv"].ToString();
                //                if (!string.IsNullOrEmpty(row["fechaini"].ToString()))
                //                    //oportunidad.FechaIni = DateTime.Parse(row["fechaini"].ToString());
                //                    oportunidad.FechaIni = DateTime.ParseExact(row["fechaini"].ToString(), "MM/dd/yyyy",
                //                                                        System.Globalization.CultureInfo.InvariantCulture);
                //                if (!string.IsNullOrEmpty(row["fechafin"].ToString()))
                //                    //oportunidad.FechaFin = DateTime.Parse(row["fechafin"].ToString());
                //                    oportunidad.FechaFin = DateTime.ParseExact(row["fechafin"].ToString(), "MM/dd/yyyy",
                //                                                        System.Globalization.CultureInfo.InvariantCulture);
                //                oportunidad.genera = byte.Parse(row["genera"].ToString());
                //                oportunidad.xml = xmlBase64;
                //                if (!string.IsNullOrEmpty(row["IdCategoria"].ToString()))
                //                    oportunidad.IdCategoria = int.Parse(row["IdCategoria"].ToString());
                //                else
                //                    oportunidad.IdCategoria = null;
                //                entities.Oportunidades.AddObject(oportunidad);
                //            }
                //            else
                //            {
                //                foreach (Oportunidades oportunidad in query)
                //                {
                //                    oportunidad.NOportunidad = row["noportunidad"].ToString();
                //                    oportunidad.Media = row["media"].ToString();
                //                    oportunidad.Ddv = row["ddv"].ToString();
                //                    if (!string.IsNullOrEmpty(row["fechaini"].ToString()))
                //                        //oportunidad.FechaIni = DateTime.Parse(row["fechaini"].ToString());
                //                        oportunidad.FechaIni = DateTime.ParseExact(row["fechaini"].ToString(), "MM/dd/yyyy",
                //                                                        System.Globalization.CultureInfo.InvariantCulture);
                //                    if (!string.IsNullOrEmpty(row["fechafin"].ToString()))
                //                        //oportunidad.FechaFin = DateTime.Parse(row["fechafin"].ToString());
                //                        oportunidad.FechaFin = DateTime.ParseExact(row["fechafin"].ToString(), "MM/dd/yyyy",
                //                                                        System.Globalization.CultureInfo.InvariantCulture);
                //                    oportunidad.genera = byte.Parse(row["genera"].ToString());
                //                    oportunidad.xml = xmlBase64;
                //                    if (!string.IsNullOrEmpty(row["IdCategoria"].ToString()))
                //                        oportunidad.IdCategoria = int.Parse(row["IdCategoria"].ToString());
                //                    else
                //                        oportunidad.IdCategoria = null;
                //                }
                //            }
                //            entities.SaveChanges();
                            
                //        }

                //        c=1;

                //        var queryOC = from o in entities.OportunidadesCliente
                //                      where o.IdOportunidad == IdOportunidad
                //                      && o.IdCliente == IdCliente
                //                      && o.IdRuta == IdRuta
                //                      select o;

                //        if(queryOC.Any())
                //        {
                //            foreach (var opc in queryOC)
                //            {
                //                if (!string.IsNullOrEmpty(row["IdCategoriaOportunidadCliente"].ToString()))
                //                    opc.IdCategoria = int.Parse(row["IdCategoriaOportunidadCliente"].ToString());
                //                else
                //                    opc.IdCategoria = null;
                //                if (opc.Estatus.Value.Equals(1) && !(bool)opetype)
                //                {
                //                    opc.Estatus = 0;
                //                }
                //            }
                //        }else
                //        {
                //            OportunidadesCliente oportunidadCliente = new OportunidadesCliente();
                //            oportunidadCliente.IdOportunidad = IdOportunidad;
                //            oportunidadCliente.IdCliente = IdCliente;
                //            oportunidadCliente.IdRuta = int.Parse(row["idruta"].ToString());
                //            oportunidadCliente.FecRegistro = FechaRegistro;
                //            oportunidadCliente.Estatus = 0;
                //            if (!string.IsNullOrEmpty(row["IdCategoriaOportunidadCliente"].ToString()))
                //                oportunidadCliente.IdCategoria = int.Parse(row["IdCategoriaOportunidadCliente"].ToString());
                //            else
                //                oportunidadCliente.IdCategoria = null;
                //            ////entities.OportunidadesCliente.AddObject(oportunidadCliente);
                //            oportunidadescliente.Add(oportunidadCliente);
                //        }

                //        SaveImages(IdOportunidad);
                //    }

                //    string con = ConfigurationManager.ConnectionStrings["DWHSTD_Connection"].ConnectionString;
                //    entities.SaveChanges();
                //    SqlBulkCopy bulkCopy = new SqlBulkCopy(con, SqlBulkCopyOptions.Default);
                //    bulkCopy.BulkCopyTimeout = 360000;
                //    if (oportunidadescliente.Count > 0)
                //    {
                //        bulkCopy.DestinationTableName = "OportunidadesCliente";
                //        bulkCopy.WriteToServer(oportunidadescliente.AsDataReader());
                //    }
                //    bulkCopy.Close();
                #endregion
                }
                return "TRUE";
            }
            catch (Exception ex)
            {
                return "Error: " + ex.Message;
            }
        }

        public string seguimientoOportIncumplida(string xmlBase64, string fechaRegistro)
        {            
            try
            {
                bool huboCambios = false;
                CultureInfo cultureProvider = new CultureInfo("es-MX");
                DateTime FechaRegistro;
                if (!string.IsNullOrEmpty(fechaRegistro))
                    FechaRegistro = DateTime.ParseExact(fechaRegistro, "MM/dd/yyyy HH:mm",
                                                        System.Globalization.CultureInfo.InvariantCulture);
                else
                    FechaRegistro = DateTime.Now;

                DayOfWeek dia = FechaRegistro.DayOfWeek;

                byte[] encodedDataAsBytes = Convert.FromBase64String(xmlBase64);
                string returnValue = ASCIIEncoding.ASCII.GetString(encodedDataAsBytes);

                XmlDocument xDoc = new XmlDocument();
                xDoc.LoadXml(returnValue);

                Engine motor = new Engine();
                DataSet ds = motor.dameOportunidadesCondicion(xDoc, fechaRegistro, EstatusOportunidad.Cumplidas);
                DataTable table = ds.Tables[0];
                EnumerableRowCollection<DataRow> TableOportunidades = table.AsEnumerable().OrderBy(o => o.Field<int>("idruta"));
                List<OportunidadesCliente> oportunidadescliente = new List<OportunidadesCliente>();
                // var yu = TableOportunidades.Select(p=>p[Opo])
                using (DWHSTDEntities entities = new DWHSTDEntities())
                {
                    //entities.Connection.Open(); 
                    string connectionString = ConfigurationManager.ConnectionStrings["BebidasDesLP_Connection"].ConnectionString;
                    SQLManejador conexionLP = new SQLManejador(connectionString);

                    foreach (DataRow row in TableOportunidades)
                    {

                        Guid IdOportunidad = Guid.Parse(row["IdOportunidad"].ToString());
                        int IdCliente = int.Parse(row["IdCliente"].ToString());
                        int IdRuta = int.Parse(row["IdRuta"].ToString());
                        //int genera = int.Parse(row["genera"].ToString());

                        #region Borrame
                        DataSet dsOpeType = new DataSet();
                        dsOpeType.Tables.Add("resulset");
                        if (conexionLP.AbrirConexion())
                        {
                            conexionLP.Instruccion = "select ope_type from ope_oportunidaddescuento where ope_oportunidaddescuentoId='" + row["IdOportunidad"] + "'";
                            dsOpeType.Merge(conexionLP.EjecutarConsulta(dsOpeType.Tables["resulset"]));
                            conexionLP.CerrarConexion();
                        }

                        var opetype = false; //TEMPORAL dsOpeType.Tables["resulset"].Rows[0]["ope_type"];                        
                        #endregion                        

                        var queryOC = from o in entities.OportunidadesCliente
                                      where o.IdOportunidad == IdOportunidad
                                      && o.IdCliente == IdCliente
                                      && o.IdRuta == IdRuta                                      
                                      && o.FecRegistro.Year == FechaRegistro.Year
                                      && o.FecRegistro.Month == FechaRegistro.Month
                                      && o.FecRegistro.Day == FechaRegistro.Day
                                      && o.FecRegistro.Hour == FechaRegistro.Hour
                                      && o.FecRegistro.Minute == FechaRegistro.Minute
                                      select o;

                        if (queryOC.Any())
                        {

                            foreach (var opc in queryOC)
                            {
                                if (opc.Estatus.Value.Equals(0) /*&& !(bool)opetype*/)
                                {
                                    opc.Estatus = 2;
                                    opc.UltCambio = DateTime.Now;
                                    huboCambios = true;
                                }
                            }
                        }
                    }

                    if (huboCambios)
                    {
                        string con = ConfigurationManager.ConnectionStrings["DWHSTD_Connection"].ConnectionString;
                        entities.SaveChanges();
                        SqlBulkCopy bulkCopy = new SqlBulkCopy(con, SqlBulkCopyOptions.Default);
                        if (oportunidadescliente.Count > 0)
                        {
                            bulkCopy.DestinationTableName = "OportunidadesCliente";
                            bulkCopy.WriteToServer(oportunidadescliente.AsDataReader());
                        }
                        bulkCopy.Close();
                    }
                }

                return "TRUE";
            }
            catch (Exception ex)
            {
                return "Error: " + ex.Message;
            }

        }

        public string seguimientoOportCumplida(string xmlBase64, string fechaRegistro)
        {
            try
            {
                bool huboCambios = false;
                CultureInfo cultureProvider = new CultureInfo("es-MX");
                DateTime FechaRegistro;
                if (!string.IsNullOrEmpty(fechaRegistro))
                    FechaRegistro = DateTime.ParseExact(fechaRegistro, "MM/dd/yyyy HH:mm",
                                                        System.Globalization.CultureInfo.InvariantCulture);
                else
                    FechaRegistro = DateTime.Now;

                DayOfWeek dia = FechaRegistro.DayOfWeek;

                byte[] encodedDataAsBytes = Convert.FromBase64String(xmlBase64);
                string returnValue = ASCIIEncoding.ASCII.GetString(encodedDataAsBytes);

                XmlDocument xDoc = new XmlDocument();
                xDoc.LoadXml(returnValue);

                Engine motor = new Engine();
                DataSet ds = motor.dameOportunidadesCondicion(xDoc, fechaRegistro, EstatusOportunidad.Cumplidas);
                DataTable table = ds.Tables[0];
                EnumerableRowCollection<DataRow> TableOportunidades = table.AsEnumerable().OrderBy(o => o.Field<int>("idruta"));
                List<OportunidadesCliente> oportunidadescliente = new List<OportunidadesCliente>();
                // var yu = TableOportunidades.Select(p=>p[Opo])
                using (DWHSTDEntities entities = new DWHSTDEntities())
                {
                    //entities.Connection.Open(); 
                    string connectionString = ConfigurationManager.ConnectionStrings["BebidasDesLP_Connection"].ConnectionString;
                    SQLManejador conexionLP = new SQLManejador(connectionString);

                    foreach (DataRow row in TableOportunidades)
                    {

                        Guid IdOportunidad = Guid.Parse(row["IdOportunidad"].ToString());
                        int IdCliente = int.Parse(row["IdCliente"].ToString());
                        int IdRuta = int.Parse(row["IdRuta"].ToString());
                        //int genera = int.Parse(row["genera"].ToString());

                        #region Borrame
                        DataSet dsOpeType = new DataSet();
                        dsOpeType.Tables.Add("resulset");
                        if (conexionLP.AbrirConexion())
                        {
                            conexionLP.Instruccion = "select ope_type from ope_oportunidaddescuento where ope_oportunidaddescuentoId='" + row["IdOportunidad"] + "'";
                            dsOpeType.Merge(conexionLP.EjecutarConsulta(dsOpeType.Tables["resulset"]));
                            conexionLP.CerrarConexion();
                        }

                        var opetype = false; //TEMPORAL dsOpeType.Tables["resulset"].Rows[0]["ope_type"];                        
                        #endregion

                        var queryOC = from o in entities.OportunidadesCliente
                                      where o.IdOportunidad == IdOportunidad
                                      && o.IdCliente == IdCliente
                                      && o.IdRuta == IdRuta
                                      && o.FecRegistro.Year == FechaRegistro.Year
                                      && o.FecRegistro.Month == FechaRegistro.Month
                                      && o.FecRegistro.Day == FechaRegistro.Day
                                      && o.FecRegistro.Hour == FechaRegistro.Hour
                                      && o.FecRegistro.Minute == FechaRegistro.Minute
                                      select o;

                        if (queryOC.Any())
                        {

                            foreach (var opc in queryOC)
                            {
                                if (opc.Estatus.Value.Equals(1) /*&& !(bool)opetype*/)
                                {
                                    opc.Estatus = 2;
                                    opc.UltCambio = DateTime.Now;
                                    huboCambios = true;
                                }
                            }
                        }
                    }

                    if (huboCambios)
                    {
                        string con = ConfigurationManager.ConnectionStrings["DWHSTD_Connection"].ConnectionString;
                        entities.SaveChanges();
                        SqlBulkCopy bulkCopy = new SqlBulkCopy(con, SqlBulkCopyOptions.Default);
                        if (oportunidadescliente.Count > 0)
                        {
                            bulkCopy.DestinationTableName = "OportunidadesCliente";
                            bulkCopy.WriteToServer(oportunidadescliente.AsDataReader());
                        }
                        bulkCopy.Close();
                    }
                }

                return "TRUE";
            }
            catch (Exception ex)
            {
                return "Error: " + ex.Message;
            }

        }

        public void actualizaOportunidad(string xmlBase64)
        {
            byte[] encodedDataAsBytes = System.Convert.FromBase64String(xmlBase64);
            string returnValue = System.Text.ASCIIEncoding.ASCII.GetString(encodedDataAsBytes);

            XmlDocument xDoc = new XmlDocument();
            xDoc.LoadXml(returnValue);
            entidad estr = new entidad();
            XmlNodeReader reader = new XmlNodeReader(xDoc.DocumentElement);
            XmlSerializer ser = new XmlSerializer(estr.GetType());
            object obj = ser.Deserialize(reader);

            entidad entidad = (entidad)obj;

            try
            {
                using (DWHSTDEntities entities = new DWHSTDEntities())
                {
                    Guid IdOportunidad = Guid.Parse(entidad.taskid);                    

                    //busca la oportunidad para ver si existe
                    var query = from o in entities.Oportunidades
                                where o.IdOportunidad == IdOportunidad
                                select o;
                    if (query.Count() == 0)
                    {
                        Oportunidades oportunidad = new Oportunidades();
                        oportunidad.IdOportunidad = IdOportunidad;
                        oportunidad.NOportunidad = entidad.task;
                        oportunidad.Media = entidad.media;
                        oportunidad.Ddv = entidad.ddv;
                        if (!string.IsNullOrEmpty(entidad.fechaini))
                            //oportunidad.FechaIni = DateTime.Parse(row["fechaini"].ToString());
                            oportunidad.FechaIni = DateTime.ParseExact(entidad.fechaini, "MM/dd/yyyy",
                                                                System.Globalization.CultureInfo.InvariantCulture);
                        if (!string.IsNullOrEmpty(entidad.fechafin))
                            //oportunidad.FechaFin = DateTime.Parse(row["fechafin"].ToString());
                            oportunidad.FechaFin = DateTime.ParseExact(entidad.fechafin, "MM/dd/yyyy",
                                                                System.Globalization.CultureInfo.InvariantCulture);
                        oportunidad.genera = byte.Parse(entidad.genera);
                        oportunidad.xml = xmlBase64;
                        entities.Oportunidades.AddObject(oportunidad);
                    }
                    else
                    {
                        foreach (Oportunidades oportunidad in query)
                        {
                            oportunidad.NOportunidad = entidad.task;
                            oportunidad.Media = entidad.media;
                            oportunidad.Ddv = entidad.ddv;
                            if (!string.IsNullOrEmpty(entidad.fechaini))
                                //oportunidad.FechaIni = DateTime.Parse(row["fechaini"].ToString());
                                oportunidad.FechaIni = DateTime.ParseExact(entidad.fechaini, "MM/dd/yyyy",
                                                                    System.Globalization.CultureInfo.InvariantCulture);
                            if (!string.IsNullOrEmpty(entidad.fechafin))
                                //oportunidad.FechaFin = DateTime.Parse(row["fechafin"].ToString());
                                oportunidad.FechaFin = DateTime.ParseExact(entidad.fechafin, "MM/dd/yyyy",
                                                                    System.Globalization.CultureInfo.InvariantCulture);
                            oportunidad.genera = byte.Parse(entidad.genera);
                            oportunidad.xml = xmlBase64;
                        }
                    }
                    entities.SaveChanges();

                    SaveImages(IdOportunidad);
                }
                    

            }
            catch (Exception ex)
            {
                //return "Error: " + ex.Message;
            }

        }

        private void SaveImages(Guid IdOportunidad)
        {
            try
            {
                string LocalFile = ConfigurationManager.ConnectionStrings["LocalFile"].ConnectionString;
                var CRMEntities = new CRMEntities();
                var Annotations = CRMEntities.Annotation.Where(a => a.ObjectId.Value.Equals(IdOportunidad));

                foreach (var a in Annotations)
                {
                    var bytes = Convert.FromBase64String(a.DocumentBody);
                    using (var imageFile = new FileStream(LocalFile + a.FileName, FileMode.OpenOrCreate, FileAccess.Write))
                    {
                        imageFile.Write(bytes, 0, bytes.Length);
                        imageFile.Flush();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Models.Annotation> GetAnnotations(Guid ObjectId)
        {
            List<Models.Annotation> Annotation = new List<Models.Annotation>();
            try
            {
                var CRMEntities = new CRMEntities();
                var CRMAnnotations = CRMEntities.Annotation.Where(a => a.ObjectId.Value.Equals(ObjectId));

                foreach (var a in CRMAnnotations)
                {
                    Annotation.Add(new Models.Annotation { MimeType = a.MimeType, DocumentBody = a.DocumentBody, Subject = a.Subject, AnnotationId = a.AnnotationId, FileName = a.FileName });
                }
            }
            catch (Exception ex)
            {

            }
            return Annotation;
        }

        public string GetCensos()
        {
            reglaCenso objCenso = new reglaCenso();
            return objCenso.fnGetCensos();
        }

        public string GetPreguntasPorCensos(int idcenso)
        {
            reglaCenso objCenso = new reglaCenso();
            return objCenso.fnGetPreguntasPorCensos(idcenso);
        }

        public string Borrame(string xmlBase64)
        {
            
            return xmlBase64;
        }

        private Oportunidades fillOportunity(Oportunidades oportunity, DataRow row)
        {
            oportunity.NOportunidad = row["noportunidad"].ToString();
            oportunity.Media = row["media"].ToString();
            oportunity.Ddv = row["ddv"].ToString();
            if (!string.IsNullOrEmpty(row["fechaini"].ToString()))
                oportunity.FechaIni = DateTime.ParseExact(row["fechaini"].ToString(), "MM/dd/yyyy",
                                                    System.Globalization.CultureInfo.InvariantCulture);
            if (!string.IsNullOrEmpty(row["fechafin"].ToString()))
                oportunity.FechaFin = DateTime.ParseExact(row["fechafin"].ToString(), "MM/dd/yyyy",
                                                    System.Globalization.CultureInfo.InvariantCulture);
            oportunity.genera = byte.Parse(row["genera"].ToString());
            if (!string.IsNullOrEmpty(row["IdCategoria"].ToString()))
                oportunity.IdCategoria = int.Parse(row["IdCategoria"].ToString());
            else
                oportunity.IdCategoria = null;

            return oportunity;
        }

        private void mayBulkInsert(List<OportunidadesCliente> oportunidadescliente)
        {
            if (ListUtils.isValidList(oportunidadescliente))
            {
                bulkInsert(oportunidadescliente);
            }
        }

        private void bulkInsert(List<OportunidadesCliente> oportunidadescliente)
        {
            string con = ConfigurationManager.ConnectionStrings["DWHSTD_Connection"].ConnectionString;
            SqlConnection conn = new SqlConnection(con);
            conn.Open();
            using (var trans = conn.BeginTransaction())
            {
                try
                {
                    var initDate = DateTime.Now;
                    conn.Execute("INSERT INTO OportunidadesCliente (IdOportunidad, IdCliente, IdRuta, FecRegistro, Estatus, IdCategoria, NCedis, NCategoria, IdSupervisor, NSupervisor, IdJefe, NJefe, IdCedis) " +
                        "VALUES (@IdOportunidad, @IdCliente, @IdRuta, @FecRegistro, @Estatus, @IdCategoria, @NCedis, @NCategoria, @IdSupervisor, @NSupervisor, @IdJefe, @NJefe, @IdCedis)",
                        oportunidadescliente, trans);
                    var finishDate = DateTime.Now;
                }
                catch (Exception e)
                {
                    trans.Rollback();
                    throw;
                }

                trans.Commit();
            }
        }

        private void mayBulkUpdate(List<OportunidadesCliente> oportunidadescliente)
        {
            if (ListUtils.isValidList(oportunidadescliente))
            {
                bulkUpdate(oportunidadescliente);
            }
        }

        private void bulkUpdate(List<OportunidadesCliente> oportunidadescliente)
        {
            string con = ConfigurationManager.ConnectionStrings["DWHSTD_Connection"].ConnectionString;
            SqlConnection conn = new SqlConnection(con);
            conn.Open();
            using (var trans = conn.BeginTransaction())
            {
                try
                {
                    var initDate = DateTime.Now;
                    conn.Execute("UPDATE OportunidadesCliente SET IdOportunidad = @IdOportunidad, IdCliente = @IdCliente, IdRuta = @IdRuta, FecRegistro = @FecRegistro, " +
                        "Estatus = @Estatus, IdCategoria = @IdCategoria, NCedis = @NCedis, NCategoria = @NCategoria, IdSupervisor = @IdSupervisor, NSupervisor = @NSupervisor, " +
                        "IdJefe = @IdJefe, NJefe = @NJefe, IdCedis = @IdCedis " + 
                        "WHERE IdOportunidadCliente = @IdOportunidadCliente", 
                        oportunidadescliente, trans);
                    var finishDate = DateTime.Now;
                }
                catch (Exception e)
                {
                    trans.Rollback();
                    throw;
                }

                trans.Commit();
            }
        }

        private List<OportunidadesCliente> convertToList(EnumerableRowCollection<DataRow> TableOportunidades)
        {
            DateTime fechaRegistro = DateTime.Now;
            List<OportunidadesCliente> oportunidadescliente = new List<OportunidadesCliente>();
            oportunidadescliente = TableOportunidades.Select(item => new OportunidadesCliente()
                            {
                                IdOportunidad = Guid.Parse(item["oportunidadid"].ToString()),
                                IdCliente = int.Parse(item["idcliente"].ToString()),
                                IdRuta = int.Parse(item["idruta"].ToString()),
                                FecRegistro = fechaRegistro,
                                Estatus = 0,
                                NCedis = item["NCedis"].ToString(),
                                NCategoria = item["NCategoria"].ToString(),
                                IdSupervisor = int.Parse(item["IdSupervisor"].ToString()),
                                NSupervisor = item["NSupervisor"].ToString(),
                                IdJefe = int.Parse(item["IdJefe"].ToString()),
                                NJefe = item["NJefe"].ToString(),
                                IdCedis = int.Parse(item["IdCedis"].ToString()),
                                IdCategoria = !string.IsNullOrEmpty(item["IdCategoriaOportunidadCliente"].ToString()) ? 
                                                    int.Parse(item["IdCategoriaOportunidadCliente"].ToString()) : (Nullable<Int32>)null
                            }).ToList();  
            return oportunidadescliente;
        }
    }
}
