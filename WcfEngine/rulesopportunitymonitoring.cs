﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace WcfEngine
{
    public class rulesopportunitymonitoring
    {
        public rulesopportunitymonitoring()
        { }

        [XmlElement("rule")]
        public List<rule> rule { get; set; }
    }
}