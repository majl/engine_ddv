﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Xml;
using System.Collections;
using System.Xml.Serialization;
using System.Configuration;
using WcfEngine.Utils;
using WcfEngine.Controllers;
using WcfEngine.Models;
using WcfEngine.Utils;

namespace WcfEngine
{
    public enum EstatusOportunidad : int { Cumplidas = 0, Incumplidas = 1 };

    public class Engine
    {
        #region Constantes
        private const string LAST_MONTH = "last-month";
        private const string THIS_MONTH = "this-month";
        private const string LAST_X_MONTHS = "last-x-months";
        #endregion
        #region Variables
        private DateTime dtFechaNow;
        private Hashtable operadores;
        #endregion

        public Engine()
        {
            dtFechaNow = DateTime.Now;
            initOperadores();
        }

        public DataSet ExecuteInsert(XmlDocument xDoc)
        {
            entidad estr = new entidad();
            rulecondition rcstr = new rulecondition();

            DataSet dsResult = new DataSet();

            DataSet dsrulecondition = new DataSet();
            dsrulecondition.Tables.Add("resulset");

            XmlNodeReader reader = new XmlNodeReader(xDoc.DocumentElement);
            XmlSerializer ser = new XmlSerializer(estr.GetType());

            object obj = ser.Deserialize(reader);

            entidad entidad = (entidad)obj;

            #region Objetos

            linkentity le = null;

            #endregion

            #region Variables

            string sql = string.Empty;
            string sql_in = string.Empty;
            string sql_in2 = string.Empty;
            string sql_in3 = string.Empty;

            string sql_condition = string.Empty;

            string sql_le = string.Empty;
            string sql_le2 = string.Empty;

            string sql_where = string.Empty;
            string sql_where2 = string.Empty;
            string sql_complemento = string.Empty;
            string cadcon = string.Empty;
            #endregion

            //bool op_nulo = false;
            List<rule> reglas = null;

            if (entidad.rule.Count() > 0)
                reglas = entidad.rule;
            else
                reglas = entidad.rulesopportunityconditions.rule;

            if (reglas.Count() > 0)
            {
                //mbobadillav - 20170105 - cambio de consulta para insercion de informacion 
                sql =  " ;with CatFiguraCteValid as( " + System.Environment.NewLine;
                sql += " select IdSupervisorRutaCliente, CodigoCategoria = min(c.Codigo)  " + System.Environment.NewLine;
                sql += " from CategoriaFiguraRutaCliente cfc  " + System.Environment.NewLine;
                sql += " inner join Categoria c on cfc.IdCategoria = c.IdCategoria   " + System.Environment.NewLine;
                sql += " where  cfc.Estado = 1  " + System.Environment.NewLine;
                if (entidad.categoria != "null")
                {
                    if (entidad.categoria.Equals("1") || entidad.categoria.Equals("5"))
                    {
                        sql += " AND c.Codigo IN (1,5) " + System.Environment.NewLine;
                    }
                    else
                    {
                        sql += String.Format(" AND  c.Codigo={0} ", entidad.categoria) + System.Environment.NewLine;
                    }
                }
                sql += " group by cfc.IdSupervisorRutaCliente  " + System.Environment.NewLine;
                sql += " ) " + System.Environment.NewLine;
                sql += String.Format("SELECT cliente.idcliente, catruta.IdRuta, c.IdCedis, {0}.{1}, oportunidadid ='{2}', noportunidad = '{3}',\n", entidad.name, entidad.retrieveattribute, entidad.taskid, entidad.task);
                sql += String.Format("       media = '{0}', ddv = '{1}', \n       fechaini = '{2}', fechafin = '{3}', genera = {4}, \n", entidad.media, entidad.ddv, entidad.fechaini, entidad.fechafin, entidad.genera);
                sql += " cat.IdCategoria As IdCategoria , \n";
                sql += " c.NCedis, cat.NCategoria, IdSupervisor = ISNULL(s.IdSupervisor, 0), ISNULL(s.nsupervisor, 'No se cuenta con Supervisor') NSupervisor, \n";
                sql += " IdJefe = ISNULL(jefesup.IdJefe, 0), ISNULL(jefesup.nomjefe, 'No se cuenta con Jefe') NJefe, \n";
                sql += " IdCategoriaOportunidadCliente = cat.IdCategoria " + System.Environment.NewLine;
                sql += "  FROM " + entidad.name + " (nolock) " + System.Environment.NewLine;
                sql += "inner join rutacliente catrutacliente (nolock) on cliente.idcliente = catrutacliente.idcliente and catrutacliente.Estatus=1" + System.Environment.NewLine;
                sql += "inner join ruta catruta (nolock) on catrutacliente.idruta = catruta.idruta and catruta.Estatus=1" + System.Environment.NewLine;
                sql += "inner join SupervisorRutaCliente src (nolock) on src.IdRutaCliente = catrutacliente.IdRutaCliente and src.Estatus=1 " + System.Environment.NewLine;
                sql += "inner join Supervisor s (nolock) on s.idsupervisor = src.idsupervisor and s.Estatus=1 " + System.Environment.NewLine;
                sql += "inner join CatFiguraCteValid sups on sups.IdSupervisorRutaCliente = src.IdSupervisorRutaCliente " + System.Environment.NewLine;
                sql += "inner join Categoria cat (nolock) on cat.Codigo = sups.CodigoCategoria " + System.Environment.NewLine;
                sql += "INNER JOIN Cedis c ON c.IdCedis = catruta.IdCedis " + System.Environment.NewLine;
                sql += "LEFT JOIN jefesup jefesup ON jefesup.idsupervisor = s.supervisor and jefesup.idcedis = c.idcedis " + System.Environment.NewLine;

                sql = sql.Replace("{", "").Replace("}", "");

                foreach (rule regla in reglas)
                {
                    sql_where = string.Empty;

                    if (regla.rulecondition.Count() > 0)
                    {
                        sql_where = obtenSqlCondicion(entidad, regla);

                        sql_where += getIntensidadCompetitiva(entidad, regla, sql_where);
                        //sql_where += getCedisCondition(entidad, regla, sql_where);
                        sql_where += " AND Cliente.Estatus = 1 ";
                        cadcon = ConfigurationManager.ConnectionStrings["DWHSTD_Connection"].ConnectionString;
                        SQLManejador conexion = new SQLManejador(cadcon);

                        if (conexion.AbrirConexion())
                        {
                            conexion.Instruccion = sql + sql_where;// +sql_complemento;
                            DataTable dataTable = conexion.EjecutarConsulta(dsrulecondition.Tables["resulset"]);
                            var groups = dataTable.AsEnumerable()
                                .GroupBy(x => x, new DistinctInsertClientComparer()).ToList();
                            List<DataRow> result = groups.Select(group => group.Key).ToList();
                            dataTable.Rows.Cast<DataRow>()
                                .Where(d => !result.Contains(d))
                                .ToList()
                                .ForEach(r => r.Delete());
                            dataTable.AcceptChanges();
                            if (dataTable != null && DataUtils.isValidDataSet(dataTable.DataSet))
                            {
                                if (entidad.categoria != "null")
                                {
                                    if (entidad.categoria.Equals("1") || entidad.categoria.Equals("5"))
                                    {
                                        dataTable = filterClientBE(dataTable, entidad.categoria);
                                    }
                                }
                                dataTable = getVentaCubo(entidad, regla, dataTable);
                            }
                            dsrulecondition.Merge(dataTable);
                        }
                    }
                }
            }

            DataView Dv = dsrulecondition.Tables["resulset"].DefaultView;
            dsResult.Tables.Add(Dv.ToTable(true, "idruta", "idcliente", "oportunidadid", "noportunidad", "media", "ddv", "fechaini", "fechafin", "genera", "IdCategoria", 
                "IdCedis", "NCedis", "NCategoria", "IdSupervisor", "NSupervisor", "IdJefe", "NJefe", "IdCategoriaOportunidadCliente"));

            return dsResult;
        }

        public DataSet dameOportunidadesCondicion(XmlDocument xDoc, string stFechaRegistro, EstatusOportunidad Estatus)
        {
            if (!string.IsNullOrEmpty(stFechaRegistro))
                dtFechaNow = DateTime.ParseExact(stFechaRegistro, "MM/dd/yyyy HH:mm",
                                                    System.Globalization.CultureInfo.InvariantCulture);
            entidad estr = new entidad();
            rulecondition rcstr = new rulecondition();

            DataSet dsResult = new DataSet();

            DataSet dsrulecondition = new DataSet();
            dsrulecondition.Tables.Add("resulset");

            XmlNodeReader reader = new XmlNodeReader(xDoc.DocumentElement);
            XmlSerializer ser = new XmlSerializer(estr.GetType());

            object obj = ser.Deserialize(reader);

            entidad entidad = (entidad)obj;

            #region Variables

            string sql = string.Empty;
            string sql_in = string.Empty;
            string sql_in2 = string.Empty;
            string sql_in3 = string.Empty;

            string sql_condition = string.Empty;

            string sql_le = string.Empty;
            string sql_le2 = string.Empty;

            string sql_where = string.Empty;
            string sql_where2 = string.Empty;
            string sql_complemento = string.Empty;
            string cadcon = string.Empty;
            #endregion

            //bool op_nulo = false;
            List<rule> reglas = null;

            reglas = entidad.rulesopportunitymonitoring.rule;

            if (reglas.Count() > 0)
            {
                sql = "SELECT oc.IdOportunidad, oc.IdCliente, oc.IdRuta, oc.Estatus, \r\n ";
                sql += "o.NOportunidad, Cliente.NCliente, oc.FecRegistro, oc.Estatus, o.FechaIni, o.FechaFin \r\n ";
                sql += "from OportunidadesCliente oc \r\n ";
                sql += "inner join Cliente Cliente on Cliente.IdCliente=oc.IdCliente \r\n ";
                sql += "inner join Oportunidades o on o.IdOportunidad=oc.IdOportunidad \r\n ";

                foreach (rule regla in reglas)
                {
                    sql_where = string.Empty;

                    if (regla.rulecondition.Count() > 0)
                    {
                        if (Estatus == EstatusOportunidad.Cumplidas)
                            sql_where = obtenSqlCondicion(entidad, regla, true, true, entidad.fechafin + " 00:00");
                        else if (Estatus == EstatusOportunidad.Incumplidas)
                            sql_where = obtenSqlCondicion(entidad, regla, false, true, entidad.fechafin + " 00:00");
                        sql_where += string.Format(" And oc.IdOportunidad='{0}' ", entidad.taskid);
                        if (!string.IsNullOrEmpty(stFechaRegistro))
                            sql_where += string.Format("And (YEAR(oc.FecRegistro)='{0}' And MONTH(oc.FecRegistro)='{1}' And DAY(oc.FecRegistro)='{2}' And DATEPART(hour,oc.FecRegistro)='{3}' And DATEPART(minute,oc.FecRegistro)='{4}')", dtFechaNow.Year, dtFechaNow.Month, dtFechaNow.Day, dtFechaNow.Hour, dtFechaNow.Minute);
                        cadcon = ConfigurationManager.ConnectionStrings["DWHSTD_Connection"].ConnectionString;
                        SQLManejador conexion = new SQLManejador(cadcon);
                        if (conexion.AbrirConexion())
                        {
                            conexion.Instruccion = sql + sql_where + sql_complemento;
                            dsrulecondition.Merge(conexion.EjecutarConsulta(dsrulecondition.Tables["resulset"]));
                        }
                    }
                }
            }


            DataView Dv = dsrulecondition.Tables["resulset"].DefaultView;
            dsResult.Tables.Add(Dv.ToTable(true, "IdOportunidad", "IdCliente", "IdRuta", "FecRegistro", "Estatus"));

            return dsResult;
        }

        public DataSet ExecutePreview(XmlDocument xDoc)
        {
            entidad estr = new entidad();
            rulecondition rcstr = new rulecondition();

            DataSet dsResult = new DataSet();

            DataSet dsrulecondition = new DataSet();
            dsrulecondition.Tables.Add("resulset");

            XmlNodeReader reader = new XmlNodeReader(xDoc.DocumentElement);
            XmlSerializer ser = new XmlSerializer(estr.GetType());

            object obj = ser.Deserialize(reader);

            entidad entidad = (entidad)obj;

            #region Objetos

            linkentity le = null;

            #endregion

            #region Variables

            string sql = string.Empty;
            string sql_in = string.Empty;
            string sql_in2 = string.Empty;
            string sql_in3 = string.Empty;

            string sql_condition = string.Empty;

            string sql_le = string.Empty;
            string sql_le2 = string.Empty;

            string sql_where = string.Empty;
            string sql_where2 = string.Empty;
            string sql_complemento = string.Empty;
            string sql_final = string.Empty;
            string cadcon = string.Empty;
            #endregion

            //bool op_nulo = false;

            List<rule> reglas = null;

            if (entidad.rule.Count() > 0)
                reglas = entidad.rule;
            else
                reglas = entidad.rulesopportunityconditions.rule;

            if (reglas.Count() > 0)
            {
           
               
                //mbobadillav - 20161230 - Cambiamos el método en que trae la categoría para la cual aplicará la oportunidad, cuando 
                //el supervisor tenga más de una categoría tomará la prioritaria (el MIN)
                sql = " ;with CatFiguraCteValid as( " + System.Environment.NewLine;
	            sql += " select IdSupervisorRutaCliente, CodigoCategoria = min(c.Codigo) " + System.Environment.NewLine;
	            sql += " from CategoriaFiguraRutaCliente cfc " + System.Environment.NewLine;
	            sql += " inner join Categoria c on cfc.IdCategoria = c.IdCategoria  " + System.Environment.NewLine;

                if (entidad.categoria != "null")
                {
                    if (entidad.categoria.Equals("1") || entidad.categoria.Equals("5"))
                    {
                        sql += " AND c.Codigo IN (1, 5) " + System.Environment.NewLine;
                    }
                    else
                    {
                        sql += String.Format(" AND c.Codigo={0}", entidad.categoria) + System.Environment.NewLine;
                    }
                }

	            sql += " where  cfc.Estado = 1 " + System.Environment.NewLine;
	            sql += " group by cfc.IdSupervisorRutaCliente " + System.Environment.NewLine;
                sql += " ) " + System.Environment.NewLine;
                sql += " SELECT DISTINCT  " + System.Environment.NewLine;
                sql += " 	   ISNULL(jefesup.nomjefe, 'No se cuenta con Jefe') nomjefe, " + System.Environment.NewLine;
                sql += " 	   ISNULL(s.nsupervisor, 'No se cuenta con Supervisor') nomsupervisor, " + System.Environment.NewLine;
                sql += " 	   ISNULL(cat.NCategoria, 'Sin categoria') NCategoria,   " + System.Environment.NewLine;
                sql += " 	   catcedis.IdCedis, catcedis.ncedis,  " + System.Environment.NewLine;
                sql += " 	   catruta.ruta, catruta.nruta, " + System.Environment.NewLine;
                sql += " 	   Cliente.idcliente, Cliente.ncliente,  " + System.Environment.NewLine;
                sql += "        Cliente.direccion, catgiro.ngiro, cattamanio.ntamanio, catpotencial.npotencial, Cliente.cajas, Cliente.skus, crtm.NCanalRTM,  " + System.Environment.NewLine;
                sql += " 	   IdCategoria = ISNULL(cat.IdCategoria, 0),  Cliente.AccountId " + System.Environment.NewLine;
                sql += " FROM Cliente (nolock)  " + System.Environment.NewLine;
                sql += " inner join Giro catgiro (nolock) on Cliente.IdGiro = catgiro.IdGiro " + System.Environment.NewLine;
                sql += " inner join Tamanio cattamanio (nolock) on cliente.idtamanio = cattamanio.idtamanio " + System.Environment.NewLine;
                sql += " inner join Potencial catpotencial (nolock) on cliente.idpotencial = catpotencial.idpotencial " + System.Environment.NewLine;
                sql += " inner join rutacliente catrutacliente (nolock) on cliente.idcliente = catrutacliente.idcliente and catrutacliente.Estatus=1 " + System.Environment.NewLine;
                sql += " inner join ruta catruta (nolock) on catrutacliente.idruta = catruta.idruta and catruta.Estatus=1 " + System.Environment.NewLine;
                sql += " inner join SupervisorRutaCliente src (nolock) on src.IdRutaCliente = catrutacliente.IdRutaCliente and src.Estatus=1  " + System.Environment.NewLine;
                sql += " inner join Supervisor s (nolock) on s.idsupervisor = src.idsupervisor and s.Estatus=1  " + System.Environment.NewLine;
                sql += " inner join CatFiguraCteValid sups on sups.IdSupervisorRutaCliente = src.IdSupervisorRutaCliente " + System.Environment.NewLine;
                sql += " inner join Categoria cat (nolock) on cat.Codigo = sups.CodigoCategoria  " + System.Environment.NewLine;
                sql += " inner join CanalRTM crtm (nolock) on crtm.IdCanalRTM = Cliente.IdCanalRTM  " + System.Environment.NewLine;
                sql += " inner join Cedis catcedis (nolock) ON catcedis.IdCedis = catruta.IdCedis  " + System.Environment.NewLine;
                sql += " left join jefesup jefesup ON jefesup.idsupervisor = s.supervisor and jefesup.idcedis = catcedis.idcedis  " + System.Environment.NewLine;
       


                foreach (rule regla in reglas)
                {
                    sql_where = string.Empty;


                    if (regla.rulecondition.Count() > 0)
                    {
                        sql_where = obtenSqlCondicion(entidad, regla);
                        sql_where += getIntensidadCompetitiva(entidad, regla, sql_where);
                        sql_where += " and Cliente.Estatus = 1 " + System.Environment.NewLine; 
        
                        //sql_where += getCedisCondition(entidad, regla, sql_where);
                        sql_complemento = " ORDER BY catcedis.ncedis, catruta.ruta,catruta.nruta, Cliente.idcliente " + System.Environment.NewLine;

                        cadcon = ConfigurationManager.ConnectionStrings["DWHSTD_Connection"].ConnectionString;
                        SQLManejador conexion = new SQLManejador(cadcon);
                        if (conexion.AbrirConexion())
                        {
                            conexion.Instruccion = sql + sql_where + sql_complemento;// +sql_final;

                            DataTable dataTable = conexion.EjecutarConsulta(dsrulecondition.Tables["resulset"]);
                            var groups = dataTable.AsEnumerable()
                                .GroupBy(x => x, new DistinctClientComparer()).ToList();
                            List<DataRow> result = groups.Select(group => group.Key).ToList();
                            dataTable.Rows.Cast<DataRow>()
                                .Where(d => !result.Contains(d))
                                .ToList()
                                .ForEach(r => r.Delete());
                            dataTable.AcceptChanges();
                            if (dataTable != null && DataUtils.isValidDataSet(dataTable.DataSet))
                            {
                                if (entidad.categoria != "null")
                                {
                                    if (entidad.categoria.Equals("1") || entidad.categoria.Equals("5"))
                                    {
                                        dataTable = filterClientBE(dataTable, entidad.categoria);                                        
                                    }
                                }
                                dataTable = getVentaCubo(entidad, regla, dataTable);
                            }
                            dsrulecondition.Merge(dataTable);
                        }
                    }
                }
            }


            DataView Dv = dsrulecondition.Tables["resulset"].DefaultView;
            dsResult.Tables.Add(Dv.ToTable(true, "nomjefe", "nomsupervisor", "NCategoria", "ruta", "nruta", "idcliente", "ncliente", "direccion", "ngiro", "ntamanio", "npotencial", "cajas", "skus", "NCanalRTM", "ncedis", "AccountId"));
            return dsResult;
        }

        private string obtenSqlCondicion(entidad entidad, rule regla, bool logicaNegada = false, bool ismonitoringcondition = false, string sfecvencimiento = "")
        {
            bool op_nulo = false;
            string sql_where = string.Empty;
            string sql_le = string.Empty;
            string sql_le2 = string.Empty;
            string sql_condition = string.Empty;
            string sql_in = string.Empty;
            string sql_in2 = string.Empty;
            string sql_in3 = string.Empty;
            Hashtable operadores = new Hashtable();
            linkentity le = null;

            operadores.Add("eq", " = ");
            operadores.Add("in", " in (");
            operadores.Add("may", " > ");
            operadores.Add("men", " < ");
            operadores.Add("noteq", " <> ");
            //1 es >, 2 es >=, 3 es =, 4 es < y 5 es <=
            operadores.Add("1", ">");
            operadores.Add("2", ">=");
            operadores.Add("3", "=");
            operadores.Add("4", "<");
            operadores.Add("5", "<=");


            if (regla != null && regla.rulecondition.Count() > 0)
            {
                foreach (rulecondition rulecondition in regla.rulecondition)
                {
                    sql_le = string.Empty;
                    sql_condition = string.Empty;
                    sql_in2 = string.Empty;

                    foreach (condition condicion in rulecondition.condition)
                    {
                        sql_in2 = string.Empty;

                        if (!condicion.operatortype.Equals("null"))
                        {
                            if (!String.IsNullOrEmpty(sql_condition))
                                sql_condition += " and ";

                            if (condicion.valuelist.Count() == 0)
                            {
                                if (fnEsCondicionFecha(condicion))
                                {
                                    sql_condition += fnDameCondicionFecha(entidad.name, condicion, ismonitoringcondition, sfecvencimiento);
                                }
                                else
                                {
                                    if (!String.IsNullOrEmpty(condicion.entity))
                                    {
                                        sql_condition += condicion.entity + "." + condicion.attribute + operadores[condicion.operatortype].ToString() + condicion.value;
                                    }
                                    else
                                    {
                                        sql_condition += entidad.name + "." + condicion.attribute + operadores[condicion.operatortype].ToString() + condicion.value;
                                    }
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(condicion.entity))
                                {
                                    sql_condition += condicion.entity + "." + condicion.attribute + operadores[condicion.operatortype].ToString();
                                }
                                else
                                {
                                    sql_condition += entidad.name + "." + condicion.attribute + operadores[condicion.operatortype].ToString();
                                }
                                foreach (string value in condicion.valuelist)
                                {
                                    sql_in2 += String.IsNullOrEmpty(sql_in2) ? value : "," + value;
                                }
                                sql_in2 += ")";

                                sql_condition += sql_in2;
                            }
                        }
                        else
                            op_nulo = logicaNegada ? false : true;
                    }

                    if (rulecondition.linkentity != null)
                    {
                        le = rulecondition.linkentity;
                        op_nulo = false;
                        sql_le = "select * from " + le.name + " (nolock) ";
                        sql_le2 = string.Empty;
                        sql_in3 = string.Empty;

                        #region region link de link
                        foreach (linkentity le1 in le.linkentitie)
                        {
                            sql_le += " inner join ";
                            for (int i = 0; i < le1.from.Count(); i++)
                            {
                                sql_le += (i > 0 ? " and " : le1.name + " on ");

                                sql_le += le.name + "." + le1.from.ElementAt(i);
                                sql_le += " = " + le1.name + "." + le1.to.ElementAt(i);
                            }

                            foreach (condition condicion in le1.condition)
                            {
                                sql_in3 = string.Empty;

                                if (!condicion.operatortype.Equals("null"))
                                {
                                    if (condicion.valuelist.Count() == 0)
                                    {
                                        sql_le2 += (string.IsNullOrEmpty(sql_le2)) ? " Where " : "   and ";
                                        if (fnEsCondicionFecha(condicion))
                                        {
                                            sql_le2 += fnDameCondicionFecha(le1.name, condicion, ismonitoringcondition, sfecvencimiento);
                                        }
                                        else
                                        {
                                            sql_le2 += le1.name + "." + condicion.attribute + operadores[condicion.operatortype].ToString() + condicion.value;
                                        }
                                    }
                                    else
                                    {
                                        if (!String.IsNullOrEmpty(sql_le2))
                                            sql_le2 += " and ";
                                        else
                                            sql_le2 += " Where ";

                                        sql_le2 += le1.name + "." + condicion.attribute + operadores[condicion.operatortype].ToString();
                                        foreach (string value in condicion.valuelist)
                                        {
                                            sql_in3 += String.IsNullOrEmpty(sql_in3) ? value : "," + value;
                                        }
                                        sql_in3 += ")";

                                        sql_le2 += sql_in3;
                                    }
                                }
                                else
                                    op_nulo = true;
                            }
                        }

                        #endregion

                        sql_le += (string.IsNullOrEmpty(sql_le2)) ? " Where " : sql_le2 + " and ";

                        sql_in = string.Empty;

                        for (int i = 0; i < le.from.Count(); i++)
                        {
                            sql_le += (i > 0 ? " and " : "");

                            sql_le += entidad.name + "." + le.from.ElementAt(i);
                            sql_le += " = " + le.name + "." + le.to.ElementAt(i);
                        }

                        foreach (condition condicion in le.condition)
                        {
                            sql_in = string.Empty;

                            if (!condicion.operatortype.Equals("null"))
                            {
                                if (condicion.valuelist.Count() == 0)
                                {
                                    if (fnEsCondicionFecha(condicion))
                                    {
                                        sql_le += " and " + fnDameCondicionFecha(le.name, condicion, true, sfecvencimiento);
                                    }
                                    else
                                    {
                                        sql_le += " and " + le.name + "." + condicion.attribute + operadores[condicion.operatortype].ToString() + condicion.value;
                                    }
                                }
                                else
                                {
                                    if (!String.IsNullOrEmpty(sql_le))
                                        sql_le += " and ";

                                    sql_le += le.name + "." + condicion.attribute + operadores[condicion.operatortype].ToString();
                                    foreach (string value in condicion.valuelist)
                                    {
                                        sql_in += String.IsNullOrEmpty(sql_in) ? value : "," + value;
                                    }
                                    sql_in += ")";

                                    sql_le += sql_in;
                                }
                            }
                            else
                                op_nulo = logicaNegada ? false : true;
                        }
                    }

                    if (rulecondition.condition.Count > 0 || rulecondition.linkentity != null)
                    {
                        if (string.IsNullOrEmpty(sql_le))
                        {
                            if (!string.IsNullOrEmpty(sql_condition)) sql_condition += System.Environment.NewLine;
                            sql_where += (string.IsNullOrEmpty(sql_where)) ? " WHERE " + sql_condition : "   AND " + sql_condition;
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(sql_condition)) sql_condition += System.Environment.NewLine + "   AND ";
                            sql_where += (string.IsNullOrEmpty(sql_where)) ? " WHERE " + sql_condition : "   AND " + sql_condition;
                            sql_where += (op_nulo ? "not " : "") + "exists (" + sql_le + ")" + System.Environment.NewLine;
                        }
                    }
                }

            }
            return sql_where;
        }

        private String getCedisCondition(entidad entidad, rule regla, String sql_where)
        {
            String sql = String.Empty;
            if (regla != null && regla.rulecondition.Count() > 0)
            {
                foreach (rulecondition rulecondition in regla.rulecondition)
                {
                    if (rulecondition.linkentity != null)
                    {
                        linkentity le = rulecondition.linkentity;
                        if (le.name.ToLower().Equals("rutacliente") || le.name.Contains("rutacliente"))
                        {
                            linkentity le2 = le.linkentitie[0];
                            condition con = le2.condition[0];
                            foreach (string value in con.valuelist)
                            {
                                sql += String.IsNullOrEmpty(sql) ? value : "," + value;
                            }
                            sql = "catruta.IdCedis IN (" + sql + ")";
                        }
                    }
                }
            }
            if (!String.IsNullOrEmpty(sql))
            {
                sql = (string.IsNullOrEmpty(sql_where)) ? " WHERE " + sql : "   AND " + sql;
            }
            return sql;
        }

        private DataTable filterClientBE(DataTable dataTable, String IdCategoria)
        {
            if (DataUtils.isValidDataSet(dataTable.DataSet))
            {
                ClienteBEController clienteBEController = new ClienteBEController();
                var clientBEList = clienteBEController.getIdClientsBE();
                if (clientBEList != null)
                {
                    if (IdCategoria.Equals("1"))
                    {
                        dataTable.Rows.Cast<DataRow>()
                            .Where(d => clientBEList.Contains(d.Field<int>("idcliente")) && d.Field<int>("IdCategoria") == 6)
                            .ToList()
                            .ForEach(r => r.Delete());
                    }
                    else if (IdCategoria.Equals("5"))
                    {
                        dataTable.Rows.Cast<DataRow>()
                            .Where(d => clientBEList.Contains(d.Field<int>("idcliente")) && d.Field<int>("IdCategoria") == 1)
                            .ToList()
                            .ForEach(r => r.Delete());
                    }
                    dataTable.AcceptChanges();
                }
            }
            return dataTable;
        }

        private DataTable getVentaCubo(entidad entidad, rule regla, DataTable dataTable)
        {
            if (regla != null && ListUtils.isValidList(regla.ventaCubos))
            {
                List<VentaCubo> list = regla.ventaCubos;
                list.ForEach(ventaCubo =>
                {
                    if (ventaCubo.linkentity != null && ListUtils.isValidList(ventaCubo.linkentity.condition))
                    {
                        linkentity link = ventaCubo.linkentity;
                        List<condition> conditions = link.condition;
                        condition conditionComparativo = conditions.Where(cond => cond.attribute.Equals("Comparativo")).First();
                        condition conditionCondicion = conditions.Where(cond => cond.attribute.Equals("Condicion")).First();
                        condition conditionCrecimiento = conditions.Where(cond => cond.attribute.Equals("Crecimiento")).First();
                        condition conditionPorcentaje = conditions.Where(cond => cond.attribute.Equals("Porcentaje")).First();

                        condition conditionProductos = conditions.Where(cond => cond.attribute.Equals("Productos")).First();
                        String productsIds = String.Empty;
                        if (conditionProductos != null && ListUtils.isValidList(conditionProductos.valuelist))
                        {
                            productsIds = conditionProductos.valuelist.Aggregate((i, j) => i + "," + j);
                        }
                        condition conditionBranches = conditions.Where(cond => cond.attribute.Equals("IdCedis")).First();

                        if (conditionCondicion != null)
                        {
                            List<int> idClientsList = new List<int>();

                            var concatClients = String.Join(",", dataTable.AsEnumerable().ToList().Select(client => client.Field<int>("IdCliente")));

                            ClientSalesController clientSalesController = new ClientSalesController();
                            if (conditionCondicion.value.Equals("MMAA"))
                            {
                                condition conditionMes = conditions.Where(cond => cond.attribute.Equals("Mes")).First();
                                condition conditionAnio = conditions.Where(cond => cond.attribute.Equals("Anio")).First();

                                List<Client> clients = clientSalesController.getClientsMMAA(ventaCubo.linkentity.name, concatClients, Int32.Parse(conditionMes.value),
                                    Int32.Parse(conditionAnio.value), conditionComparativo.value, conditionCrecimiento.value == "0" ? true : false,
                                    Int32.Parse(conditionPorcentaje.value), conditionBranches.value, productsIds);
                                if (clients != null)
                                {
                                    idClientsList.AddRange(clients.Select(c => c.IdClient));
                                }
                            }
                            else if (conditionCondicion.value.Equals("PM3"))
                            {
                                List<Client> clients = clientSalesController.getClientsPM3(ventaCubo.linkentity.name, concatClients, conditionComparativo.value,
                                    conditionCrecimiento.value == "0" ? true : false, Int32.Parse(conditionPorcentaje.value), conditionBranches.value, productsIds);
                                if (clients != null)
                                {
                                    idClientsList.AddRange(clients.Select(c => c.IdClient));
                                }
                            }
                            
                            dataTable.Rows.Cast<DataRow>()
                                .Where(d => !idClientsList.Contains(d.Field<int>("idcliente")))
                                .ToList()
                                .ForEach(r => r.Delete());
                            dataTable.AcceptChanges();
                        }
                    }
                });
            }
            return dataTable;
        }

        private String getIntensidadCompetitiva(entidad entidad, rule regla, String sql_where)
        {
            String sqlGlobal = String.Empty;
            if (regla != null && ListUtils.isValidList(regla.intensidadCompetitivas))
            {

                List<IntensidadCompetitiva> list = regla.intensidadCompetitivas;
                foreach (IntensidadCompetitiva intensidadCompetitiva in list)
                {
                    if (intensidadCompetitiva != null && intensidadCompetitiva.linkentity != null)
                    {
                        linkentity link = intensidadCompetitiva.linkentity;
                        String sql = String.Empty;
                        String sqlFromTo = String.Empty;
                        String sqlHead = String.Empty;
                        sqlFromTo = " WHERE ";
                        for (int i = 0; i < link.from.Count(); i++)
                        {
                            sqlFromTo += (i > 0 ? " and " : "");

                            sqlFromTo += entidad.name + "." + link.from.ElementAt(i);
                            sqlFromTo += " = " + link.name + "." + link.to.ElementAt(i);
                        }
                        if (link.condition != null && link.condition.Count() > 0)
                        {
                            foreach (condition condicion in link.condition)
                            {
                                if (condicion.tipo != null && condicion.tipo.Equals("suma"))
                                {
                                    sqlHead = " EXISTS (SELECT " + link.name + ".IdCliente FROM " + link.name + " (nolock) ";
                                    String sql_values = "";
                                    Set set = condicion.setList[0];
                                    if (set.valuelist.Count() > 0)
                                    {
                                        sql_values += " in (";
                                        foreach (String value in set.valuelist)
                                        {
                                            sql_values += value + ",";
                                        }
                                        sql_values = sql_values.Substring(0, sql_values.Length - 1) + ")";
                                    }
                                    sql += " and " + link.name + "." + condicion.attribute + sql_values;
                                    sql += " GROUP BY " + link.name + ".IdCliente ";
                                    sql += " HAVING SUM(" + link.name + ".Valor) " + operadores[condicion.operatortype] + condicion.value + ")";
                                }
                                else if (condicion.tipo != null && condicion.tipo.Equals("participacion"))
                                {
                                    String sqlResultado = String.Empty;
                                    String sqlPersonal = String.Empty;
                                    String sqlCompetencia = String.Empty;

                                    Set set = condicion.setList[0];
                                    sqlPersonal += " SELECT SUM(" + link.name + ".Valor) FROM " + link.name + " (nolock) " + System.Environment.NewLine;
                                    sqlPersonal += sqlFromTo + System.Environment.NewLine;
                                    sqlPersonal += sql + System.Environment.NewLine;
                                    String sql_values = "";
                                    if (set.valuelist.Count() > 0)
                                    {
                                        sql_values += " in (";
                                        foreach (String value in set.valuelist)
                                        {
                                            sql_values += value + ",";
                                        }
                                        sql_values = sql_values.Substring(0, sql_values.Length - 1) + ")";
                                    }
                                    sqlPersonal += " and " + link.name + "." + condicion.attribute + sql_values + System.Environment.NewLine;
                                    sqlPersonal += " and IntensidadCompetitiva.Valor <> 0 " + System.Environment.NewLine;
                                    sqlPersonal += " GROUP BY " + link.name + ".IdCliente" + System.Environment.NewLine;

                                    set = condicion.setList[1];
                                    sqlCompetencia += " SELECT SUM(" + link.name + ".Valor) FROM " + link.name + " (nolock) " + System.Environment.NewLine;
                                    sqlCompetencia += sqlFromTo + System.Environment.NewLine;
                                    sqlCompetencia += sql + System.Environment.NewLine;
                                    sql_values = "";
                                    if (set.valuelist.Count() > 0)
                                    {
                                        sql_values += " in (";
                                        foreach (String value in set.valuelist)
                                        {
                                            sql_values += value + ",";
                                        }
                                        sql_values = sql_values.Substring(0, sql_values.Length - 1) + ")";
                                    }
                                    sqlCompetencia += " and " + link.name + "." + condicion.attribute + sql_values + System.Environment.NewLine;
                                    sqlCompetencia += " and IntensidadCompetitiva.Valor <> 0 " + System.Environment.NewLine;
                                    sqlCompetencia += " GROUP BY " + link.name + ".IdCliente" + System.Environment.NewLine;

                                    sql = "(" + Environment.NewLine +
                                          "CASE WHEN (SELECT COALESCE(( " + Environment.NewLine +
                                          "" + sqlPersonal + "" +
                                          "),0)) = 0 THEN 0 ELSE " + Environment.NewLine +
                                          "CASE WHEN (SELECT COALESCE(( " + Environment.NewLine +
                                          "" + sqlCompetencia + "" +
                                          "),0)) = 0 THEN 1 ELSE ( " + Environment.NewLine +
                                          "(" + Environment.NewLine +
                                              "SELECT COALESCE(( " + Environment.NewLine +
                                                "" + sqlPersonal + "" +
                                              "),0) " + Environment.NewLine +
                                          ") * 1.0 " + Environment.NewLine +
                                          "/" + Environment.NewLine +
                                          "(" + Environment.NewLine +
                                            "(" + Environment.NewLine +
                                                "SELECT COALESCE(( " + Environment.NewLine +
                                                    "" + sqlPersonal + "" + Environment.NewLine +
                                                "),0)" + Environment.NewLine +
                                            ") * 1.0 " + Environment.NewLine +
                                          "+" + Environment.NewLine +
                                            "(" + Environment.NewLine +
                                                "SELECT COALESCE(( " + Environment.NewLine +
                                                    "" + sqlCompetencia + "" + Environment.NewLine +
                                                "),0)" + Environment.NewLine +
                                            ")" + Environment.NewLine +
                                          ")" + Environment.NewLine +
                                          ") END END " + Environment.NewLine +
                                          ") * 100 " + operadores[condicion.operatortype] + condicion.value + "" + Environment.NewLine +
                                    " AND ((SELECT COALESCE((" + Environment.NewLine +
                                                "" + sqlPersonal + "" + Environment.NewLine +
                                          "),0)) + (SELECT COALESCE((" + Environment.NewLine +
                                                "" + sqlCompetencia + "" + Environment.NewLine +
                                          "),0))) > 0 ";
                                    sqlFromTo = String.Empty;
                                    sqlHead = String.Empty;
                                }
                                else
                                {
                                    sql += " AND " + link.name + "." + condicion.attribute + operadores[condicion.operatortype] + condicion.value;
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(sql))
                        {
                            sql = sqlHead + sqlFromTo + sql;
                            sqlGlobal += (string.IsNullOrEmpty(sql_where)) ? " WHERE " + sql : "   AND " + sql;
                        }
                    }
                }
            }
            return sqlGlobal;
        }

        private bool fnEsCondicionFecha(condition condicion)
        {
            return condicion.operatortype.Equals(LAST_MONTH) || condicion.operatortype.Equals(THIS_MONTH) || condicion.operatortype.Equals(LAST_X_MONTHS);
        }

        private string fnDameCondicionFecha(string tabla, condition condicion, bool ismonitoringcondition = false, string sfecvencimiento = "")
        {
            string sMes = "";
            string sAnio = "";
            string sCondicionSQL = "";
            DateTime fecha = dtFechaNow;

            if (ismonitoringcondition && !string.IsNullOrEmpty(sfecvencimiento))
            {
                fecha = DateTime.ParseExact(sfecvencimiento, "MM/dd/yyyy HH:mm",
                                                    System.Globalization.CultureInfo.InvariantCulture);
            }

            if (condicion.operatortype.Equals(THIS_MONTH))
            {
                sMes = "=" + fecha.Month.ToString();
                sAnio = "=" + fecha.Year.ToString();
            }
            else if (condicion.operatortype.Equals(LAST_MONTH))
            {
                if (fecha.Month - 1 == 0)
                {
                    sMes = "=" + Convert.ToString(12);
                    sAnio = "=" + (fecha.Year - 1).ToString();
                }
                else
                {
                    sMes = "=" + Convert.ToString(fecha.Month - 1);
                    sAnio = "=" + fecha.Year.ToString();
                }
            }
            else if (condicion.operatortype.Equals(LAST_X_MONTHS))
            {
                int iMax = Convert.ToInt32(condicion.value);
                int iMes = fecha.Month;

                for (int i = 0; i < iMax; i++)
                {
                    iMes--;
                    if (!String.IsNullOrEmpty(sMes)) sMes += ",";
                    sMes += iMes.ToString();
                }

                sMes = " in (" + sMes + ")";
                sAnio = "=" + fecha.Year.ToString();
            }
            sCondicionSQL = tabla + ".Mes" + sMes + " and " + tabla + ".Anio" + sAnio;

            return sCondicionSQL;

        }

        private void initOperadores()
        {
            operadores = new Hashtable();
            operadores.Add("eq", " = ");
            operadores.Add("in", " in (");
            operadores.Add("may", " > ");
            operadores.Add("men", " < ");
            operadores.Add("noteq", " <> ");
            //1 es >, 2 es >=, 3 es =, 4 es < y 5 es <=
            operadores.Add("1", ">");
            operadores.Add("2", ">=");
            operadores.Add("3", "=");
            operadores.Add("4", "<");
            operadores.Add("5", "<=");
        }

    }
}