﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WcfEngine.Models;
using WcfEngine.Utils;
using System.Data;
using WcfEngine.Dtos;

namespace WcfEngine.Controllers
{
    public class BusinessDaysController
    {
        public List<GetBusinessDay> getBusinessDays(String idBranch, int month, int year)
        {
            String query = "SELECT IdCedis, Fecha, Habil " + Environment.NewLine +
                           " FROM DiasHabiles " + Environment.NewLine +
                           " WHERE YEAR(Fecha) = " + year + " AND MONTH(Fecha) = " + month + " AND IdCedis IN (" + idBranch + ")";
            
            SQLController sqlController = new SQLController();
            DataTable dataTable = sqlController.executeQuery(query);
            if (dataTable != null && DataUtils.isValidDataSet(dataTable.DataSet))
            {
                DataSet dataSet = dataTable.DataSet;
                var myList = dataSet.Tables["resultset"].AsEnumerable().Select(r => new GetBusinessDay
                {
                    IdBranch = r.Field<int>("IdCedis"),
                    Date = r.Field<String>("Fecha"),
                    Business = r.Field<int>("Habil") == 0 ? false : true
                });

                return myList.ToList();
            }
            return null;
        }
    }
}