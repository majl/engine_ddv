﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using WcfEngine.Models;

namespace WcfEngine.Controllers
{
    public class ClientController
    {
        public List<Client> extractIdClient(DataTable dataTable)
        {
            return dataTable.AsEnumerable().Select(m => new Client()
                    {
                        IdClient = m.Field<int>(0)
                    }).ToList();      
        }

    }
}