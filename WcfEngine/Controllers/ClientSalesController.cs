﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WcfEngine.Models;
using System.Data;
using WcfEngine.Utils;
using WcfEngine.Dtos;

namespace WcfEngine.Controllers
{
    public class ClientSalesController
    {
        //public Client getClientsMMAA(int IdClient, int month, int year, String saleType, Boolean increase, int percentaje, int IdBranch, String productIds)
        //{
        //    String sql = "if object_id('tempdb.dbo.#currentData') is not null drop table dbo.#currentData " + Environment.NewLine +
        //                 "SELECT vc.IdCliente, currentSum = ( " + Environment.NewLine +
        //                 "  SELECT ISNULL(SUM(vc1." + saleType + "),0) " + Environment.NewLine +
        //                 "  FROM VentaCubo vc1 (nolock)" + Environment.NewLine +
        //                 "  WHERE vc1.IdCliente = vc.IdCliente AND vc1." + saleType + " > 0 AND vc1.Mes = " + month + " AND vc1.Anio = " + year + " " + Environment.NewLine;
        //                 if (!String.IsNullOrEmpty(productIds))
        //                 {
        //                     sql += " AND vc1.IdProducto IN (" + productIds + ") " + Environment.NewLine;
        //                 }
        //          sql += " ) " + Environment.NewLine + 
        //                 "INTO #currentData " + Environment.NewLine +
        //                 "FROM VentaCubo vc (nolock)" + Environment.NewLine +
        //                 "WHERE vc.IdCliente = " + IdClient + Environment.NewLine + 
        //                 "GROUP BY vc.IdCliente; " + Environment.NewLine;

        //    if (month == DateTime.Now.Month && year == DateTime.Now.Year)
        //    {
        //        var businessDays = getBusinessDays(IdBranch, month, year);
        //        var currrentBusinessDays = getBusinessDays(IdBranch, DateTime.Now.Month, DateTime.Now.Year);
        //        var afterBusinessDays = getBusinessDays(IdBranch, month, year, DateTime.Now);

        //        sql += "if object_id('tempdb.dbo.#pastData') is not null drop table dbo.#pastData " + Environment.NewLine +
        //              "SELECT vc.IdCliente, pastSum = ( " + Environment.NewLine +
        //              "       SELECT CASE WHEN SUM(vc1." + saleType + ") IS NULL THEN 0 ELSE (SUM(vc1." + saleType + ") / " + currrentBusinessDays + ") * " + afterBusinessDays + " END " + Environment.NewLine +
        //              "         FROM VentaCubo vc1 (nolock) " + Environment.NewLine +
        //              "         WHERE vc1.IdCliente = vc.IdCliente " + Environment.NewLine +
        //              "         AND vc1.Mes = " + month + " AND vc1.Anio = " + (year - 1) + " AND vc1." + saleType + " > 0  " + Environment.NewLine;
        //        if (!String.IsNullOrEmpty(productIds))
        //        {
        //            sql += " AND vc1.IdProducto IN (" + productIds + ") " + Environment.NewLine;
        //        }
        //        sql += "     ) " + Environment.NewLine +
        //               "INTO #pastData " + Environment.NewLine +
        //               "FROM VentaCubo vc (nolock)" + Environment.NewLine +
        //               "WHERE vc.IdCliente = " + IdClient + Environment.NewLine +
        //               " GROUP BY vc.IdCliente;" + Environment.NewLine;

        //        sql += "if object_id('tempdb.dbo.#paso1') is not null drop table dbo.#paso1 " + Environment.NewLine +
        //               "SELECT pd.IdCliente, pd.pastSum, cd.currentSum, " + Environment.NewLine +
        //               "        crecimiento = (CASE WHEN cd.currentSum <> 0 AND pd.pastSum = 0 THEN 1 ELSE 0 END), " + Environment.NewLine +
        //               "        decrecimiento = (CASE WHEN cd.currentSum = 0 AND pd.pastSum <> 0 THEN 1 ELSE 0 END) " + Environment.NewLine +
        //               " INTO #paso1 " + Environment.NewLine +
        //               " FROM #pastData pd " + Environment.NewLine +
        //               " INNER JOIN #currentData cd ON pd.IdCliente = cd.IdCliente " + Environment.NewLine +
        //               " ORDER BY pd.IdCliente " + Environment.NewLine +

        //               " if object_id('tempdb.dbo.#paso2') is not null drop table dbo.#paso2 " + Environment.NewLine +
        //               " SELECT idcliente,porcentaje = (CASE WHEN crecimiento = 1 THEN 100 " + Environment.NewLine +
        //               "			                            WHEN decrecimiento = 1 THEN -100  " + Environment.NewLine +
        //               "				                        WHEN pastSum <> 0 AND currentSum <> 0 THEN pastSum/currentSum END) " + Environment.NewLine +
        //               " INTO #paso2 " + Environment.NewLine +
        //               " FROM #paso1;" + Environment.NewLine;

        //        sql += "SELECT IdCliente " + Environment.NewLine +
        //               "FROM #paso2 " + Environment.NewLine;

        //        if (increase)
        //        {
        //            sql += " WHERE porcentaje -1 > 0 " + Environment.NewLine;
        //            sql += " AND (porcentaje -1) * 100 >= " + percentaje + " " + Environment.NewLine;
        //        }
        //        else
        //        {
        //            sql += " WHERE porcentaje -1 < 0 " + Environment.NewLine;
        //            sql += " AND (porcentaje -1) * 100 <= " + percentaje + " " + Environment.NewLine;
        //        }

        //        sql += " ORDER BY IdCliente" + Environment.NewLine;

        //    }
        //    else
        //    {
        //        sql += "if object_id('tempdb.dbo.#paso') is not null drop table dbo.#paso " + Environment.NewLine +
        //               "SELECT vc.IdCliente, pastSum = ( " + Environment.NewLine +
        //                 "  SELECT ISNULL(SUM(vc1." + saleType + "),0) " + Environment.NewLine +
        //                 "  FROM VentaCubo vc1 (nolock)" + Environment.NewLine +
        //                 "  WHERE vc1.IdCliente = vc.IdCliente AND vc1." + saleType + " > 0 AND vc1.Mes = " + month + " AND vc1.Anio = " + (year - 1) + " " + Environment.NewLine;
        //        if (!String.IsNullOrEmpty(productIds))
        //        {
        //            sql += " AND vc1.IdProducto IN (" + productIds + ") " + Environment.NewLine;
        //        }
        //        sql += " ), cData.currentSum " + Environment.NewLine +
        //               "INTO #paso " + Environment.NewLine +
        //               "FROM VentaCubo vc (nolock)" + Environment.NewLine +
        //               "INNER JOIN #currentData cData ON cData.IdCliente = vc.IdCliente " + Environment.NewLine +
        //               "WHERE vc.IdCliente = " + IdClient + Environment.NewLine +
        //               "GROUP BY vc.IdCliente, cData.currentSum " + Environment.NewLine;
        //        if (increase)
        //        {
        //            sql += "HAVING (cData.currentSum / SUM(vc." + saleType + ")) -1 > 0 " + Environment.NewLine;
        //            sql += "AND ((cData.currentSum / SUM(vc." + saleType + ")) -1) * 100 >= " + percentaje + " " + Environment.NewLine;
        //        }
        //        else
        //        {
        //            sql += "HAVING (cData.currentSum / SUM(vc." + saleType + ")) -1 < 0 " + Environment.NewLine;
        //            sql += "AND ((cData.currentSum / SUM(vc." + saleType + ")) -1) * 100 <= " + percentaje + " " + Environment.NewLine;
        //        }

        //        sql += "ORDER BY vc.IdCliente;" + Environment.NewLine;

        //        sql += "SELECT IdCliente FROM #paso " + Environment.NewLine +
        //               "WHERE pastSum <> 0 OR currentSum <> 0;" + Environment.NewLine;
        //    }

        //    SQLController sqlController = new SQLController();
        //    DataTable dataTable = sqlController.executeQuery(sql);
        //    if (dataTable != null && DataUtils.isValidDataSet(dataTable.DataSet))
        //    {
        //        var client = dataTable.AsEnumerable().Select(dr => new Client(){
        //            IdClient = dr.Field<int>(0)
        //        }).First();
        //        return client;
        //    }
        //    return null;
        //}

        public List<Client> getClientsMMAA(String entity, String IdClients, int month, int year, String saleType, 
                                            Boolean increase, int percentaje, String IdBranch, String productIds)
        {
            String sql = "if object_id('tempdb.dbo.#currentData') is not null drop table dbo.#currentData " + Environment.NewLine +
                         "SELECT vc.IdCliente, currentSum = ( " + Environment.NewLine +
                         "  SELECT ISNULL(SUM(vc1." + saleType + "),0) " + Environment.NewLine +
                         "  FROM " + entity + " vc1 (nolock)" + Environment.NewLine +
                         "  WHERE vc1.IdCliente = vc.IdCliente AND vc1." + saleType + " > 0 AND vc1.Mes = " + month + " AND vc1.Anio = " + year + " " + Environment.NewLine;
            if (!String.IsNullOrEmpty(IdBranch))
            {
               sql += "   AND vc1.puntovta IN (" + IdBranch + ") " + Environment.NewLine;
            }
            if (!String.IsNullOrEmpty(productIds))
            {
                sql += " AND vc1.IdProducto IN (" + productIds + ") " + Environment.NewLine;
            }
            sql += " ) " + Environment.NewLine +
                   "INTO #currentData " + Environment.NewLine +
                   "FROM " + entity + " vc (nolock)" + Environment.NewLine +
                   "WHERE vc.IdCliente IN (" + IdClients + ")" + Environment.NewLine +
                   "GROUP BY vc.IdCliente; " + Environment.NewLine;

            if (month == DateTime.Now.Month && year == DateTime.Now.Year)
            {
                List<BusinessDay> currrentBusinessDays = getBusinessDays(IdBranch, DateTime.Now.Month, DateTime.Now.Year);
                List<BusinessDay> afterBusinessDays = getBusinessDays(IdBranch, month, year, DateTime.Now);
                sql += "if object_id('tempdb.dbo.#tempBusinessDays') is not null drop table #tmpBusinessDays " + Environment.NewLine +
                      "CREATE TABLE #tempBusinessDays( " + Environment.NewLine +
	                  "     IdBranch int,  " + Environment.NewLine +
	                  "     Days int, " + Environment.NewLine +
	                  "     IsCurrent int " + Environment.NewLine +
                      ")" + Environment.NewLine;
                foreach(var current in currrentBusinessDays)
                {
                    sql += "INSERT INTO #tempBusinessDays (IdBranch, Days, IsCurrent) VALUES (" + current.IdBranch + ", " + current.Days + ", 1);" + Environment.NewLine;
                }
                foreach (var after in afterBusinessDays)
                {
                    sql += "INSERT INTO #tempBusinessDays (IdBranch, Days, IsCurrent) VALUES (" + after.IdBranch + ", " + after.Days + ", 0);" + Environment.NewLine;
                }

                sql += "if object_id('tempdb.dbo.#pastData') is not null drop table dbo.#pastData " + Environment.NewLine +
                      "SELECT vc.IdCliente, pastSum = ( " + Environment.NewLine +
                      "       SELECT CASE WHEN SUM(vc1." + saleType + ") IS NULL THEN 0 " + Environment.NewLine +
                      "             ELSE ( " + Environment.NewLine +
                      "                 SUM(vc1." + saleType + ") / (SELECT Days FROM #tempBusinessDays tB WHERE tb.IsCurrent = 1 AND tb.IdBranch = vc1.puntovta)) " + Environment.NewLine +
                      "                     * (SELECT Days FROM #tempBusinessDays tB WHERE tb.IsCurrent = 0 AND tB.IdBranch = vc1.puntovta) END " + Environment.NewLine +
                      "         FROM " + entity + " vc1 (nolock) " + Environment.NewLine +
                      "         WHERE vc1.IdCliente = vc.IdCliente " + Environment.NewLine +
                      "         AND vc1.Mes = " + month + " AND vc1.Anio = " + (year - 1) + " AND vc1." + saleType + " > 0  " + Environment.NewLine;
                if (!String.IsNullOrEmpty(IdBranch))
                {
                    sql += "   AND vc1.puntovta IN (" + IdBranch + ") " + Environment.NewLine;
                }
                if (!String.IsNullOrEmpty(productIds))
                {
                    sql += " AND vc1.IdProducto IN (" + productIds + ") " + Environment.NewLine;
                }
                sql += " GROUP BY vc1.puntovta " + Environment.NewLine;
                sql += "     ) " + Environment.NewLine +
                       "INTO #pastData " + Environment.NewLine +
                       "FROM " + entity + " vc (nolock)" + Environment.NewLine +
                       "WHERE vc.IdCliente IN (" + IdClients + ")" + Environment.NewLine +
                       " GROUP BY vc.IdCliente;" + Environment.NewLine;

                sql += "if object_id('tempdb.dbo.#paso1') is not null drop table dbo.#paso1 " + Environment.NewLine +
                       "SELECT pd.IdCliente, pd.pastSum, cd.currentSum, " + Environment.NewLine +
                       "        crecimiento = (CASE WHEN cd.currentSum <> 0 AND pd.pastSum = 0 THEN 1 ELSE 0 END), " + Environment.NewLine +
                       "        decrecimiento = (CASE WHEN cd.currentSum = 0 AND pd.pastSum <> 0 THEN 1 ELSE 0 END) " + Environment.NewLine +
                       " INTO #paso1 " + Environment.NewLine +
                       " FROM #pastData pd " + Environment.NewLine +
                       " INNER JOIN #currentData cd ON pd.IdCliente = cd.IdCliente " + Environment.NewLine +
                       " ORDER BY pd.IdCliente " + Environment.NewLine +

                       " if object_id('tempdb.dbo.#paso2') is not null drop table dbo.#paso2 " + Environment.NewLine +
                       " SELECT idcliente,porcentaje = (CASE WHEN crecimiento = 1 THEN 100 " + Environment.NewLine +
                       "			                            WHEN decrecimiento = 1 THEN -100  " + Environment.NewLine +
                       "				                        WHEN pastSum <> 0 AND currentSum <> 0 THEN pastSum/currentSum END) " + Environment.NewLine +
                       " INTO #paso2 " + Environment.NewLine +
                       " FROM #paso1;" + Environment.NewLine;

                sql += "SELECT IdCliente " + Environment.NewLine +
                       "FROM #paso2 " + Environment.NewLine;

                if (increase)
                {
                    sql += " WHERE porcentaje -1 > 0 " + Environment.NewLine;
                    sql += " AND (porcentaje -1) * 100 >= " + percentaje + " " + Environment.NewLine;
                }
                else
                {
                    sql += " WHERE porcentaje -1 < 0 " + Environment.NewLine;
                    sql += " AND (porcentaje -1) * 100 <= " + percentaje + " " + Environment.NewLine;
                }

                sql += " ORDER BY IdCliente" + Environment.NewLine;

            }
            else
            {
                sql += "if object_id('tempdb.dbo.#paso') is not null drop table dbo.#paso " + Environment.NewLine +
                       "SELECT vc.IdCliente, pastSum = ( " + Environment.NewLine +
                         "  SELECT ISNULL(SUM(vc1." + saleType + "),0) " + Environment.NewLine +
                         "  FROM " + entity + " vc1 (nolock)" + Environment.NewLine +
                         "  WHERE vc1.IdCliente = vc.IdCliente AND vc1." + saleType + " > 0 AND vc1.Mes = " + month + " AND vc1.Anio = " + (year - 1) + " " + Environment.NewLine;
                if (!String.IsNullOrEmpty(IdBranch))
                {
                    sql += "   AND vc1.puntovta IN (" + IdBranch + ") " + Environment.NewLine;
                }
                if (!String.IsNullOrEmpty(productIds))
                {
                    sql += " AND vc1.IdProducto IN (" + productIds + ") " + Environment.NewLine;
                }
                sql += " ), cData.currentSum " + Environment.NewLine +
                       "INTO #paso " + Environment.NewLine +
                       "FROM " + entity + " vc (nolock)" + Environment.NewLine +
                       "INNER JOIN #currentData cData ON cData.IdCliente = vc.IdCliente " + Environment.NewLine +
                       "WHERE vc.IdCliente IN (" + IdClients + ")" + Environment.NewLine +
                       "GROUP BY vc.IdCliente, cData.currentSum " + Environment.NewLine +
                       "ORDER BY vc.IdCliente;" + Environment.NewLine;

                sql +=  "SELECT IdCliente, currentSum, pastSum " + Environment.NewLine +
                        "FROM #paso " + Environment.NewLine +
                        "WHERE ( " + Environment.NewLine +
                        "    CASE WHEN currentSum = 0 THEN -1 ELSE " + Environment.NewLine +
                        "    CASE WHEN pastSum = 0 THEN 1 ELSE  " + Environment.NewLine +
                        "    ((currentSum / pastSum) - 1) END  " + Environment.NewLine +
                        "    END) * 100 " + Environment.NewLine;
                if (increase)
                {
                    sql += " >= " + percentaje + " " + Environment.NewLine;
                }
                else
                {
                    sql += " <= " + percentaje + " " + Environment.NewLine;
                }
                sql += " AND currentSum + pastSum > 0;" + Environment.NewLine;

            }

            SQLController sqlController = new SQLController();
            DataTable dataTable = sqlController.executeQuery(sql);
            if (dataTable != null && DataUtils.isValidDataSet(dataTable.DataSet))
            {
                var client = dataTable.AsEnumerable().Select(dr => new Client()
                {
                    IdClient = dr.Field<int>(0)
                });
                return client.ToList();
            }
            return null;
        }

        //public Client getClientsPM3(int IdClient, String saleType, Boolean increase, int percentaje, int IdBranch, String productIds)
        //{
        //    String sql = "if object_id('tempdb.dbo.#monthsOfSales') is not null drop table dbo.#monthsOfSales  " + Environment.NewLine +
        //                    "SELECT DISTINCT vc.IdCliente, vc.Mes " + Environment.NewLine +
        //                    "INTO #monthsOfSales " + Environment.NewLine +
        //                    "FROM VentaCubo vc (nolock)" + Environment.NewLine +
        //                    "WHERE  " + Environment.NewLine +
        //                    "	vc.Venta > 0 " + Environment.NewLine +
        //                    "	AND DATEDIFF( " + Environment.NewLine +
        //                    "		mm,  " + Environment.NewLine +
        //                    "		CONVERT(VARCHAR(10), (CONVERT(VARCHAR(4),vc.Anio) + CONVERT(VARCHAR(2),RIGHT('0'+CAST(vc.Mes AS VARCHAR(2)),2)) + '01'), 112),  " + Environment.NewLine +
        //                    "		DATEADD(MONTH, -1, GETDATE()) " + Environment.NewLine +
        //                    "	) < 3 " + Environment.NewLine +
        //                    "	AND CONVERT(VARCHAR(4), vc.Anio) + CONVERT(VARCHAR(2),RIGHT('0'+CAST(vc.Mes AS VARCHAR(2)),2)) " + Environment.NewLine +
        //                    "	<> CONVERT(VARCHAR(4),YEAR(GETDATE())) + CONVERT(VARCHAR(2),RIGHT('0'+CAST(MONTH(GETDATE()) AS VARCHAR(2)),2)) " + Environment.NewLine +
        //                    "   AND vc.IdCliente = " + IdClient + " " + Environment.NewLine +
        //                    "GROUP BY vc.IdCliente, vc.Mes " + Environment.NewLine +
        //                    "ORDER BY vc.IdCliente, vc.Mes DESC; " + Environment.NewLine;

        //    sql += "if object_id('tempdb.dbo.#currentData') is not null drop table dbo.#currentData " + Environment.NewLine +
        //                 "SELECT vc.IdCliente, SUM(vc." + saleType + ") AS currentSum " + Environment.NewLine +
        //                 "INTO #currentData " + Environment.NewLine +
        //                 "FROM VentaCubo vc (nolock)" + Environment.NewLine +
        //                 "WHERE vc." + saleType + " > 0 AND vc.Mes = MONTH(DATEADD(MONTH, -1, GETDATE())) AND vc.Anio = YEAR(DATEADD(MONTH, -1, GETDATE())) " + Environment.NewLine +
        //                 "AND vc.IdCliente = " + IdClient + " " + Environment.NewLine +
        //                 "GROUP BY vc.IdCliente; " + Environment.NewLine;

        //    sql += "SELECT vc.IdCliente " + Environment.NewLine +
        //                 "FROM VentaCubo vc (nolock) " + Environment.NewLine +
        //                 "INNER JOIN #currentData cData ON cData.IdCliente = vc.IdCliente " + Environment.NewLine +
        //                 "WHERE vc." + saleType + " > 0 " + Environment.NewLine +
        //                 "	AND DATEDIFF( " + Environment.NewLine +
        //                 "		mm,  " + Environment.NewLine +
        //                 "		CONVERT(VARCHAR(10), (CONVERT(VARCHAR(4),vc.Anio) + CONVERT(VARCHAR(2),RIGHT('0'+CAST(vc.Mes AS VARCHAR(2)),2)) + '01'), 112),  " + Environment.NewLine +
        //                 "		DATEADD(MONTH, -1, GETDATE()) " + Environment.NewLine +
        //                 "	) < 3 " + Environment.NewLine +
        //                 "	AND CONVERT(VARCHAR(4), vc.Anio) + CONVERT(VARCHAR(2),RIGHT('0'+CAST(vc.Mes AS VARCHAR(2)),2)) " + Environment.NewLine +
        //                 "	<> CONVERT(VARCHAR(4),YEAR(GETDATE())) + CONVERT(VARCHAR(2),RIGHT('0'+CAST(MONTH(GETDATE()) AS VARCHAR(2)),2)) " + Environment.NewLine +
        //                 " AND vc.IdCliente = " + IdClient + Environment.NewLine;
        //    if (!String.IsNullOrEmpty(productIds))
        //    {
        //        sql += " AND vc.IdProducto IN (" + productIds + ") " + Environment.NewLine;
        //    }
        //    sql += " GROUP BY vc.IdCliente, cData.currentSum " + Environment.NewLine;
        //    if (increase)
        //    {
        //        sql += "HAVING (cData.currentSum / (SUM(vc." + saleType + ") / (SELECT COUNT(*) FROM #monthsOfSales))) -1 > 0 " + Environment.NewLine;
        //        sql += "AND ((cData.currentSum / (SUM(vc." + saleType + ") / (SELECT COUNT(*) FROM #monthsOfSales))) -1) * 100 >= " + percentaje + " " + Environment.NewLine;
        //    }
        //    else
        //    {
        //        sql += "HAVING (cData.currentSum /(SUM(vc." + saleType + ") / (SELECT COUNT(*) FROM #monthsOfSales))) -1 < 0 " + Environment.NewLine;
        //        sql += "AND ((cData.currentSum / (SUM(vc." + saleType + ") / (SELECT COUNT(*) FROM #monthsOfSales))) -1) * 100 <= " + percentaje + " " + Environment.NewLine;
        //    }

        //    sql += "ORDER BY vc.IdCliente";


        //    SQLController sqlController = new SQLController();
        //    DataTable dataTable = sqlController.executeQuery(sql);
        //    if (dataTable != null && DataUtils.isValidDataSet(dataTable.DataSet))
        //    {
        //        var client = dataTable.AsEnumerable().Select(dr => new Client()
        //        {
        //            IdClient = dr.Field<int>(0)
        //        }).First();
        //        return client;
        //    }
        //    return null;
        //}

        public List<Client> getClientsPM3(String entity, String IdClients, String saleType, Boolean increase, int percentaje, String IdBranch, String productIds)
        {
            String sql = "if object_id('tempdb.dbo.#monthsOfSales') is not null drop table dbo.#monthsOfSales  " + Environment.NewLine +
                            "SELECT DISTINCT vc.puntovta, vc.IdCliente, vc.Mes " + Environment.NewLine +
                            "INTO #monthsOfSales " + Environment.NewLine +
                            "FROM " + entity + " vc (nolock)" + Environment.NewLine +
                            "WHERE  " + Environment.NewLine +
                            "	vc.Venta > 0 " + Environment.NewLine +
                            "	AND DATEDIFF( " + Environment.NewLine +
                            "		mm,  " + Environment.NewLine +
                            "		CONVERT(VARCHAR(10), (CONVERT(VARCHAR(4),vc.Anio) + CONVERT(VARCHAR(2),RIGHT('0'+CAST(vc.Mes AS VARCHAR(2)),2)) + '01'), 112),  " + Environment.NewLine +
                            "		DATEADD(MONTH, -1, GETDATE()) " + Environment.NewLine +
                            "	) BETWEEN 0 AND 2 " + Environment.NewLine +
                            "   AND vc.IdCliente IN (" + IdClients + ") " + Environment.NewLine;
            if(!String.IsNullOrEmpty(IdBranch))
            {
                sql += "   AND vc.puntovta IN (" + IdBranch + ") " + Environment.NewLine;
            }
            if (!String.IsNullOrEmpty(productIds))
            {
                sql +=  "   AND vc.IdProducto IN (" + productIds + ") " + Environment.NewLine;
            }
                sql += "GROUP BY vc.puntovta, vc.IdCliente, vc.Mes " + Environment.NewLine +
                        "ORDER BY vc.IdCliente, vc.Mes DESC; " + Environment.NewLine;

                sql += "if object_id('tempdb.dbo.#monthsOfSalesPast') is not null drop table dbo.#monthsOfSalesPast " + Environment.NewLine +
                        "SELECT DISTINCT vc.puntovta, vc.IdCliente, vc.Mes " + Environment.NewLine +
                        "INTO #monthsOfSalesPast " + Environment.NewLine +
                        "FROM " + entity + " vc (nolock)" + Environment.NewLine +
                        "WHERE  " + Environment.NewLine +
                        "	vc.Venta > 0 " + Environment.NewLine +
                        "	AND DATEDIFF( " + Environment.NewLine +
                        "		mm,  " + Environment.NewLine +
                        "		CONVERT(VARCHAR(10), (CONVERT(VARCHAR(4),vc.Anio) + CONVERT(VARCHAR(2),RIGHT('0'+CAST(vc.Mes AS VARCHAR(2)),2)) + '01'), 112),  " + Environment.NewLine +
                        "		CONVERT(VARCHAR(10), (CONVERT(VARCHAR(4),YEAR(GETDATE()) - 1) + CONVERT(VARCHAR(2),RIGHT('0'+CAST(MONTH(GETDATE()) - 1 AS VARCHAR(2)),2)) + '01'), 112) " + Environment.NewLine +
                        "	) BETWEEN 0 AND 2 " + Environment.NewLine +
                        "   AND vc.IdCliente IN (" + IdClients + ") " + Environment.NewLine;
            if (!String.IsNullOrEmpty(IdBranch))
            {
                sql += "   AND vc.puntovta IN (" + IdBranch + ") " + Environment.NewLine;
            }
            if (!String.IsNullOrEmpty(productIds))
            {
                sql +=  "   AND vc.IdProducto IN (" + productIds + ") " + Environment.NewLine;
            }
                sql += "GROUP BY vc.puntovta, vc.IdCliente, vc.Mes " + Environment.NewLine +
                        "ORDER BY vc.IdCliente, vc.Mes DESC; " + Environment.NewLine;

                sql += "if object_id('tempdb.dbo.#currentData') is not null drop table dbo.#currentData " + Environment.NewLine +
                            "SELECT vc.idcliente, CASE COUNT(DISTINCT ms.Mes) WHEN 0 THEN 0 ELSE SUM(vcu) / COUNT(DISTINCT ms.Mes) END average " + Environment.NewLine +
                            "INTO #currentData " + Environment.NewLine +
                            "FROM " + entity + " vc " + Environment.NewLine +
                            "LEFT JOIN  #monthsOfSales ms ON ms.IdCliente = vc.IdCliente AND vc.Mes = ms.Mes AND vc.puntovta = ms.puntovta " + Environment.NewLine +
                            "WHERE vc.idcliente IN (" + IdClients + ") " + Environment.NewLine +
                            "   AND vc.Anio = YEAR(GETDATE()) " + Environment.NewLine +
                            "   AND vc.Mes IN (SELECT DISTINCT ms.Mes) " + Environment.NewLine;
            if (!String.IsNullOrEmpty(IdBranch))
            {
                sql += "   AND vc.puntovta IN (" + IdBranch + ") " + Environment.NewLine;
            }
            if (!String.IsNullOrEmpty(productIds))
            {
                sql +=  "   AND vc.IdProducto IN (" + productIds + ") " + Environment.NewLine;
            }
            sql +=      "GROUP BY vc.IdCliente " + Environment.NewLine;

            sql += "if object_id('tempdb.dbo.#pastData') is not null drop table dbo.#pastData " + Environment.NewLine +
                        "SELECT vc.idcliente, CASE COUNT(DISTINCT ms.Mes) WHEN 0 THEN 0 ELSE SUM(vcu) / COUNT(DISTINCT ms.Mes) END average " + Environment.NewLine +
                        "INTO #pastData " + Environment.NewLine +
                        "FROM " + entity + " vc " + Environment.NewLine +
                        "LEFT JOIN  #monthsOfSalesPast ms ON ms.IdCliente = vc.IdCliente AND vc.Mes = ms.Mes AND vc.puntovta = ms.puntovta " + Environment.NewLine +
                        "WHERE vc.idcliente IN (" + IdClients + ") " + Environment.NewLine +
                        "   AND vc.Anio = YEAR(GETDATE()) - 1 " + Environment.NewLine +
                        "   AND vc.Mes IN (SELECT DISTINCT ms.Mes) " + Environment.NewLine;
            if (!String.IsNullOrEmpty(IdBranch))
            {
                sql += "   AND vc.puntovta IN (" + IdBranch + ") " + Environment.NewLine;
            }
            if (!String.IsNullOrEmpty(productIds))
            {
                sql +=  "   AND vc.IdProducto IN (" + productIds + ") " + Environment.NewLine;
            }
            sql +=      "GROUP BY vc.IdCliente " + Environment.NewLine;

            sql += "SELECT pD.IdCliente, cD.average, pD.average " + Environment.NewLine +
                   "FROM #pastData pD " + Environment.NewLine +
                   "LEFT JOIN #currentData cD ON pD.IdCliente = cD.IdCliente " + Environment.NewLine +
                   "WHERE ( " + Environment.NewLine +
	               "     CASE WHEN ISNULL(pD.average,0) = 0 THEN 1 ELSE " + Environment.NewLine +
	               "     CASE WHEN ISNULL(cD.average,0) = 0 THEN -1 ELSE " + Environment.NewLine +
                   "     ((cD.average / pD.average) - 1) END " + Environment.NewLine +
                   "     END) * 100 " + Environment.NewLine;
            if (increase)
            {
                sql += " >= " + percentaje + " " + Environment.NewLine;
            }
            else
            {
                sql += " <= " + percentaje + " " + Environment.NewLine;
            }

            sql += "ORDER BY pD.IdCliente";


            SQLController sqlController = new SQLController();
            DataTable dataTable = sqlController.executeQuery(sql);
            if (dataTable != null && DataUtils.isValidDataSet(dataTable.DataSet))
            {
                var client = dataTable.AsEnumerable().Select(dr => new Client()
                {
                    IdClient = dr.Field<int>(0)
                });
                return client.ToList();
            }
            return null;
        }

        private List<BusinessDay> getBusinessDays(String IdBranch, int month, int year, DateTime endDate)
        {
            var startDate = DateUtils.getFirstDayOfMonth(year, month);
            var sundays = DateUtils.CountDayOfWeek(startDate, endDate, DayOfWeek.Sunday);

            BusinessDaysController businessDaysController = new BusinessDaysController();
            List<GetBusinessDay> businessDay = businessDaysController.getBusinessDays(IdBranch, month, year);
            if (ListUtils.isValidList(businessDay))
            {
                var groupBranchBusinessDay = businessDay.ToList()
                    .GroupBy(b => b.IdBranch, (key, g) => new { IdBranch = key, Business = g.ToList() });
                var businesDayList = groupBranchBusinessDay.Select(group => new BusinessDay
                {
                    IdBranch = group.IdBranch,
                    Days = endDate.Day - sundays + group.Business.Where(bd => bd.Business == true).Count() - group.Business.Where(bd => bd.Business == false).Count()
                });
                return businesDayList.ToList();
            }
            var branches = IdBranch.Split(',');
            List<BusinessDay> emptyResult = new List<BusinessDay>();
            foreach (var branch in branches)
            {
                BusinessDay getBusinessDay = new BusinessDay();
                getBusinessDay.IdBranch = int.Parse(branch);
                getBusinessDay.Days = endDate.Day - sundays;
                emptyResult.Add(getBusinessDay);
            }
            return emptyResult;
        }

        private List<BusinessDay> getBusinessDays(String IdBranch, int month, int year)
        {
            var startDate = DateUtils.getFirstDayOfMonth(year, month);
            var endDate = DateUtils.getLastDayOfMonth(year, month);
            var sundays = DateUtils.CountDayOfWeek(startDate, endDate, DayOfWeek.Sunday);

            BusinessDaysController businessDaysController = new BusinessDaysController();
            List<GetBusinessDay> businessDay = businessDaysController.getBusinessDays(IdBranch, month, year);
            if (ListUtils.isValidList(businessDay))
            {
                var groupBranchBusinessDay = businessDay.ToList()
                    .GroupBy(b => b.IdBranch, (key, g) => new { IdBranch = key, Business = g.ToList() });
                var businesDayList = groupBranchBusinessDay.Select(group => new BusinessDay
                {
                    IdBranch = group.IdBranch,
                    Days = endDate.Day - sundays + group.Business.Where(bd => bd.Business == true).Count() - group.Business.Where(bd => bd.Business == false).Count()
                });
                return businesDayList.ToList();
            }
            var branches = IdBranch.Split(',');
            List<BusinessDay> emptyResult = new List<BusinessDay>();
            foreach (var branch in branches)
            {
                BusinessDay getBusinessDay = new BusinessDay();
                getBusinessDay.IdBranch = int.Parse(branch);
                getBusinessDay.Days = endDate.Day - sundays;
                emptyResult.Add(getBusinessDay);
            }
            return emptyResult;
        }
    }
}