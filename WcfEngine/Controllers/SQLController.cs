﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Configuration;

namespace WcfEngine.Controllers
{
    public class SQLController
    {
        public DataTable executeQuery(String query) 
        {
            if (!String.IsNullOrEmpty(query))
            {
                DataSet dsResult = new DataSet();
                dsResult.Tables.Add("resulset");

                String cadcon = ConfigurationManager.ConnectionStrings["DWHSTD_Connection"].ConnectionString;
                SQLManejador conexion = new SQLManejador(cadcon);
                if (conexion.AbrirConexion())
                {
                    conexion.Instruccion = query;

                    return conexion.EjecutarConsulta(dsResult.Tables["resulset"]);
                }
            }
            return null;
        }
    }
}