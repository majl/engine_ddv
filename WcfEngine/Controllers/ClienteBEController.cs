﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WcfEngine.Models;
using System.Data;
using WcfEngine.Utils;

namespace WcfEngine.Controllers
{
    public class ClienteBEController
    {
        public List<ClientBE> getClientsBE() 
        {
            SQLController sqlController = new SQLController();
            DataTable dataTable = sqlController.executeQuery("SELECT * FROM ClienteBE WHERE BE = 1");
            if (dataTable != null && DataUtils.isValidDataSet(dataTable.DataSet))
            {
                var client = dataTable.AsEnumerable().Select(dr => new ClientBE()
                {
                    IdCliente = dr.Field<int>(0)
                });
                return client.ToList();
            }
            return null;
        }

        public List<int> getIdClientsBE()
        {
            SQLController sqlController = new SQLController();
            DataTable dataTable = sqlController.executeQuery("SELECT * FROM ClienteBE WHERE BE = 1");
            if (dataTable != null && DataUtils.isValidDataSet(dataTable.DataSet))
            {
                var algo = dataTable.AsEnumerable().Select(dr => dr.Field<int>(0)).ToList();
                return algo;
            }
            return null;
        }

        public List<int> getIdClientsNotBE()
        {
            SQLController sqlController = new SQLController();
            DataTable dataTable = sqlController.executeQuery("SELECT * FROM ClienteBE WHERE BE = 0");
            if (dataTable != null && DataUtils.isValidDataSet(dataTable.DataSet))
            {
                var algo = dataTable.AsEnumerable().Select(dr => dr.Field<int>(0)).ToList();
                return algo;
            }
            return null;
        }
    }
}