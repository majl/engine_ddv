﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Data;

namespace WcfEngine
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IService1
    {

        [OperationContract]
        IList<ClientesPreview> GetPreview(string xmlBase64);

        [OperationContract]
        string SaveData(string xmlBase64, string fechaRegistro);
        
        [OperationContract]
        string seguimientoOportIncumplida(string xmlBase64, string fechaRegistro);

        [OperationContract]
        string seguimientoOportCumplida(string xmlBase64, string fechaRegistro);

        [OperationContract]
        void actualizaOportunidad(string xmlBase64);

        [OperationContract]
        List<Models.Annotation> GetAnnotations(Guid ObjectId);

        [OperationContract]
        string GetCensos();        

        [OperationContract]
        string GetPreguntasPorCensos(int idcenso);

        [OperationContract]
        string Borrame(string xmlBase64);
        
    }


   
}
