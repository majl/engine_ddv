﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace WcfEngine
{
    [XmlRoot("entity")]
    public class entidad
    {
        [XmlAttribute("name")]
        public string name { get; set; }

        [XmlElement("taskid")]
        public string taskid { get; set; }

        [XmlElement("task")]
        public string task { get; set; }

        [XmlElement("genera")]
        public string genera { get; set; }

        [XmlElement("categoria")]
        public string categoria { get; set; }

        [XmlElement("fechaini")]
        public string fechaini { get; set; }

        [XmlElement("fechafin")]
        public string fechafin { get; set; }

        [XmlElement("media")]
        public string media { get; set; }

        [XmlElement("ddv")]
        public string ddv { get; set; }

        [XmlElement("attribute")]
        public string retrieveattribute { get; set; }

        [XmlElement("order")]
        public string orderattribute { get; set; }

        [XmlElement("filter")]
        public string filtertype { get; set; }

        [XmlElement("rule")]
        public List<rule> rule { get; set; }

        [XmlElement("rulesopportunityconditions")]
        public rulesopportunityconditions rulesopportunityconditions { get; set; }

        [XmlElement("rulesopportunitymonitoring")]
        public rulesopportunitymonitoring rulesopportunitymonitoring { get; set; }

        public entidad()
        {

        }
    }
}