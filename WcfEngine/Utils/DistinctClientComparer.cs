﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace WcfEngine.Utils
{
    public class DistinctClientComparer : IEqualityComparer<DataRow>
    {
        public bool Equals(DataRow x, DataRow y)
        {
            return x.Field<String>(0) == y.Field<String>(0) &&
                    x.Field<String>(1) == y.Field<String>(1) &&
                //x.Field<String>(2) == y.Field<String>(2) &&
                    x.Field<int>(3) == y.Field<int>(3) &&
                    x.Field<String>(4) == y.Field<String>(4) &&
                    x.Field<int>(5) == y.Field<int>(5) &&
                    x.Field<String>(6) == y.Field<String>(6) &&
                    x.Field<int>(7) == y.Field<int>(7) &&
                    x.Field<String>(8) == y.Field<String>(8) &&
                    x.Field<String>(9) == y.Field<String>(9) &&
                    x.Field<String>(10) == y.Field<String>(10) &&
                    x.Field<String>(11) == y.Field<String>(11) &&
                    x.Field<String>(12) == y.Field<String>(12) &&
                    x.Field<int>(13) == y.Field<int>(13) &&
                    x.Field<int>(14) == y.Field<int>(14) &&
                    x.Field<String>(15) == y.Field<String>(15);
        }

        public int GetHashCode(DataRow obj)
        {
            //return obj.GetHashCode();
            return obj.Field<String>(0).ToString().GetHashCode() ^
                   obj.Field<String>(1).ToString().GetHashCode() ^
                //obj.Field<String>(2).ToString().GetHashCode() ^
                   obj.Field<int>(3).ToString().GetHashCode() ^
                   obj.Field<String>(4).ToString().GetHashCode() ^
                   obj.Field<int>(5).ToString().GetHashCode() ^
                   obj.Field<String>(6).ToString().GetHashCode() ^
                   obj.Field<int>(7).ToString().GetHashCode() ^
                   obj.Field<String>(8).ToString().GetHashCode() ^
                   obj.Field<String>(9).ToString().GetHashCode() ^
                   obj.Field<String>(10).ToString().GetHashCode() ^
                   obj.Field<String>(11).ToString().GetHashCode() ^
                   obj.Field<String>(12).ToString().GetHashCode() ^
                   obj.Field<int>(13).ToString().GetHashCode() ^
                   obj.Field<int>(14).ToString().GetHashCode() ^
                   obj.Field<String>(15).ToString().GetHashCode();
        }
    }
}