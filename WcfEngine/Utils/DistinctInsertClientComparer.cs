﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace WcfEngine.Utils
{
    public class DistinctInsertClientComparer : IEqualityComparer<DataRow>
    {
        public bool Equals(DataRow x, DataRow y)
        {
            return x.Field<int>(0) == y.Field<int>(0) && //IdCliente
                    x.Field<int>(1) == y.Field<int>(1) && //IdRuta
                    x.Field<int>(2) == y.Field<int>(2) && //IdCedis
                    x.Field<int>(3) == y.Field<int>(3) && //IdCliente
                    x.Field<String>(4) == y.Field<String>(4) && //OportunidadId
                    x.Field<String>(5) == y.Field<String>(5) && //NOportunidad
                    x.Field<String>(6) == y.Field<String>(6) && //media
                    x.Field<String>(7) == y.Field<String>(7) && //ddv
                    x.Field<String>(8) == y.Field<String>(8) && //fechaIni
                    x.Field<String>(9) == y.Field<String>(9) && //fechaFin
                    x.Field<int>(10) == y.Field<int>(10) && //genera
                //x.Field<int>(11) == y.Field<int>(11) &&
                    x.Field<String>(12) == y.Field<String>(12) && //NCedis
                    x.Field<String>(13) == y.Field<String>(13) && //NCategoria
                    x.Field<int?>(14) == y.Field<int?>(14) && //IdSupervisor
                    x.Field<String>(15) == y.Field<String>(15) && //NSupervisor
                    x.Field<int?>(16) == y.Field<int?>(16) && //IdJefe
                    x.Field<String>(17) == y.Field<String>(17) && //NJefe
                    x.Field<int>(18) == y.Field<int>(18); //IdCategoriaOportunidadCliente
        }

        public int GetHashCode(DataRow obj)
        {
            return obj.Field<int>(0).ToString().GetHashCode() ^
                   obj.Field<int>(1).ToString().GetHashCode() ^
                   obj.Field<int>(2).ToString().GetHashCode() ^
                   obj.Field<int>(3).ToString().GetHashCode() ^
                   obj.Field<String>(4).ToString().GetHashCode() ^
                   obj.Field<String>(5).ToString().GetHashCode() ^
                   obj.Field<String>(6).ToString().GetHashCode() ^
                   obj.Field<String>(7).ToString().GetHashCode() ^
                   obj.Field<String>(8).ToString().GetHashCode() ^
                   obj.Field<String>(9).ToString().GetHashCode() ^
                   obj.Field<int>(10).ToString().GetHashCode() ^
                //obj.Field<int>(11).ToString().GetHashCode() ^
                   obj.Field<String>(12).ToString().GetHashCode() ^
                   obj.Field<String>(13).ToString().GetHashCode() ^
                   obj.Field<int?>(14).ToString().GetHashCode() ^
                   obj.Field<String>(15).ToString().GetHashCode() ^
                   obj.Field<int?>(16).ToString().GetHashCode() ^
                   obj.Field<String>(17).ToString().GetHashCode() ^
                   obj.Field<int>(18).ToString().GetHashCode();
        }
    }
}