﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WcfEngine.Utils
{
    public class DateUtils
    {
        public static int CountDayOfWeek(DateTime startDate, DateTime endDate, DayOfWeek dayOfWeek)
        {
            int weekEndCount = 0;
            if (startDate > endDate)
            {
                DateTime temp = startDate;
                startDate = endDate;
                endDate = temp;
            }
            TimeSpan diff = endDate - startDate;
            int days = diff.Days;
            for (var i = 0; i <= days; i++)
            {
                var testDate = startDate.AddDays(i);
                if (testDate.DayOfWeek == dayOfWeek)
                {
                    weekEndCount += 1;
                }
            }
            return weekEndCount;
        }

        public static DateTime convertToDate(String date)
        {
            return DateTime.Parse(date);
        }

        public static DateTime convertToDate(int year, int month, int day)
        {
            return new DateTime(year, month, day);
        }

        public static DateTime getFirstDayOfMonth(int year, int month)
        {
            return new DateTime(year, month, 1);
        }

        public static DateTime getFirstDayOfMonth(DateTime dateTime)
        {
            return new DateTime(dateTime.Year, dateTime.Month, 1);
        }

        public static DateTime getLastDayOfMonth(int year, int month)
        {
            return new DateTime(year, month + 1, 1).AddDays(-1);
        }

        public static DateTime getLastDayOfMonth(DateTime dateTime)
        {
            return new DateTime(dateTime.Year, dateTime.Month + 1, 1).AddDays(-1);
        }

    }
}