﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;

namespace WcfEngine.Utils
{
    public class ListUtils
    {
        public static bool isValidList(object list)
        {
            var result = list as IList;
            if (result != null)
            {
                return result.Count > 0;
            }
            return false;
        }
    }
}