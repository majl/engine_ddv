﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace WcfEngine.Utils
{
    public class DataUtils
    {
        public static bool isValidDataSet(object dataSet)
        {
            if (dataSet != null)
            {
                var result = dataSet as DataSet;
                if (result != null)
                {
                    return result.Tables.Count > 0 && result.Tables[0].Rows.Count > 0;
                }
            }
            return false;
        }
    }
}