﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.Script.Serialization;
using System.Data.SqlClient;
using System.Configuration;

namespace WcfEngine
{
    public class reglaCenso
    {
        private static string sCnn = ConfigurationManager.ConnectionStrings["DWHSTD_Connection"].ConnectionString;

        public reglaCenso()
        { }

        public string fnGetCensos()
        {
            DataTable Censos = consulta_query("select idcenso,ncenso from censo");
            DataRow[] rows = Censos.Select();

            List<Censo> _censos = new List<Censo>();

            foreach (DataRow row in rows)
            {
                Censo aux = new Censo();
                aux.Idcenso = Int32.Parse(row["idcenso"].ToString());
                aux.Ncenso = row["ncenso"].ToString();
                _censos.Add(aux);
            }

            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();

            return jsonSerializer.Serialize(_censos);
        }

        public string fnGetPreguntasPorCensos(int idcenso)
        {
            DataTable Censos = consulta_query("select idcenso,idpregunta,npregunta from censopregunta where idcenso=" + idcenso.ToString());
            DataRow[] rows = Censos.Select();

            List<CensoPregunta> _censopregunta = new List<CensoPregunta>();

            foreach (DataRow row in rows)
            {
                CensoPregunta aux = new CensoPregunta();
                aux.Idcenso = Int32.Parse(row["idcenso"].ToString());
                aux.Idpregunta = Int32.Parse(row["idpregunta"].ToString());
                aux.Npregunta = row["npregunta"].ToString();
                _censopregunta.Add(aux);
            }

            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();

            return jsonSerializer.Serialize(_censopregunta);
        }

        private static DataTable consulta_query(string strSQL)
        {
            try
            {
                SqlConnection CnnBepensa;
                CnnBepensa = new SqlConnection(sCnn);
                SqlCommand cmdQuery = new SqlCommand(strSQL, CnnBepensa);
                SqlDataAdapter daOperadores = new SqlDataAdapter(cmdQuery);
                DataSet detalle = new DataSet();
                daOperadores.Fill(detalle);
                if (detalle.Tables[0].Rows.Count == 0)
                {
                    CnnBepensa.Close();
                    CnnBepensa.Dispose();
                    return detalle.Tables[0];
                }
                else
                {
                    CnnBepensa.Close();
                    CnnBepensa.Dispose();
                    return detalle.Tables[0];
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }

    public class Censo
    {
        private int idcenso;

        private string ncenso;

        public int Idcenso
        {
            get { return idcenso; }
            set { idcenso = value; }
        }

        public string Ncenso
        {
            get { return ncenso; }
            set { ncenso = value; }
        }
    }

    public class CensoPregunta
    {
        private int idcenso;
        private int idpregunta;
        private string npregunta;

        public string Npregunta
        {
            get { return npregunta; }
            set { npregunta = value; }
        }
        public int Idpregunta
        {
            get { return idpregunta; }
            set { idpregunta = value; }
        }

        public int Idcenso
        {
            get { return idcenso; }
            set { idcenso = value; }
        }
    }
}