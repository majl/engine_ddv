﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace WcfEngine
{
    public class linkentity
    {
        [XmlAttribute("name")]
        public string name { get; set; }

        [XmlElement("from")]
        public List<string> from { get; set; }

        [XmlElement("to")]
        public List<string> to { get; set; }

        [XmlElement("filter")]
        public string filtertype { get; set; }

        [XmlElement("link-entity")]
        public List<linkentity> linkentitie { get; set; }

        [XmlElement("condition")]
        public List<condition> condition { get; set; }

        public linkentity()
        {
        }
    }
}