﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace WcfEngine
{
    public class rulecondition
    {
        [XmlAttribute("id")]
        public string id { get; set; }

        [XmlElement("condition")]
        public List<condition> condition { get; set; }

        [XmlElement("link-entity")]
        public linkentity linkentity { get; set; }


        public rulecondition()
        {

        }
    }
}