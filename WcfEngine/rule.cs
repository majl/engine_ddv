﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;
using WcfEngine.Models;

namespace WcfEngine
{
    public class rule
    {
        [XmlAttribute("id")]
        public string id { get; set; }

        [XmlElement("rulecondition")]
        public List<rulecondition> rulecondition { get; set; }

        [XmlElement("intensidadcompetitiva")]
        public List<IntensidadCompetitiva> intensidadCompetitivas { get; set; }

        [XmlElement("ventacubo")]
        public List<VentaCubo> ventaCubos{ get; set; }

        public rule()
        {

        }
    }
}