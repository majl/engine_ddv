﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WcfEngine.Dtos
{
    public class GetBusinessDay
    {
        public int IdBranch { get; set; }
        public String Date { get; set; }
        public Boolean? Business { get; set; }
    }
}