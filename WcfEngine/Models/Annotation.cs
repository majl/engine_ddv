﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WcfEngine.Models
{
    public class Annotation
    {
        public string MimeType { get; set; }
        public string DocumentBody { get; set; }
        public string Subject { get; set; }
        public Guid AnnotationId { get; set; }
        public string FileName { get; set; }
    }
}