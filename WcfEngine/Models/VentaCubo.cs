﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace WcfEngine.Models
{
    public class VentaCubo
    {
        [XmlElement("link-entity")]
        public linkentity linkentity { get; set; }

        public VentaCubo()
        {
        }
    }
}