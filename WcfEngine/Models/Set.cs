﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace WcfEngine.Models
{
    public class Set
    {
        [XmlElement("value")]
        public List<string> valuelist { get; set; }

        public Set()
        {

        }
    }
}