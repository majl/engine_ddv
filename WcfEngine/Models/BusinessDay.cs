﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WcfEngine.Models
{
    public class BusinessDay
    {
        public int IdBranch { get; set; }
        public int Days { get; set; }
    }
}