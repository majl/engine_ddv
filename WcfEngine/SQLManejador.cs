﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Collections;
using System.Data;

namespace WcfEngine
{
    public class SQLManejador
    {
        //Contenedor
        //DataSet BaseDatos;
        //Instancias de control SQL
        SqlConnection Conector;
        SqlDataAdapter Adaptador;
        SqlTransaction TranControl;
        SqlCommand Comando = new SqlCommand();

        //Propiedades
        public string strInstruccion;
        public string Instruccion
        {
            get
            {
                return strInstruccion;
            }
            set
            {
                strInstruccion = value;
            }
        }
        string strConexion = "";
        public string Conexion
        {
            get { return strConexion; }
            set { strConexion = value; }
        }
        ArrayList aryErrores = new ArrayList();
        public ArrayList Errores
        {
            get { return aryErrores; }
        }
        string strTranName = string.Empty;
        public string TranName
        {
            get
            {
                return strTranName;
            }
            set
            {
                strTranName = value;
            }
        }
        int intTranStatus = 0;
        public int TranStatus
        {
            get
            {
                return intTranStatus;
            }
            set
            {
                intTranStatus = value;
            }
        }
        //Constructor        
        public SQLManejador(string CadCon)
        {
            Conexion = CadCon;
        }

        public bool AbrirConexion()
        {
            if (Conector == null)
            {
                Conector = new SqlConnection(Conexion);
            }

            try
            {
                if (Conector.State != ConnectionState.Open)
                {
                    Conector.Open();
                }
                return true;
            }
            catch (Exception ex)
            {
                Errores.Add(ex);
                return false;
            }
        }
        public bool CerrarConexion()
        {
            try
            {
                Conector.Close();
                return true;
            }
            catch (Exception ex)
            {
                Errores.Add(ex);
                return false;
            }
        }
        public int EjecutarConsulta() 
        {
                       
            Comando.CommandText = Instruccion;
            Comando.Connection = Conector;
            return Comando.ExecuteNonQuery();                                         
        }
        public object EjecutarEscalar()
        {
            if (Conector.State != ConnectionState.Open)
            {
                Conector.Open();
            }

            Adaptador = new SqlDataAdapter(Instruccion, Conector);
            object dato;
            dato = Adaptador.SelectCommand.ExecuteScalar();

            //Adaptador.SelectCommand.CommandTimeout = 0;
            //try
            //{
            //    Adaptador.Fill(Tabla);
            //}
            //catch (Exception ex)
            //{
            //    Tabla = null;
            //    Errores.Add(ex);
            //}
            return dato;
        }
        public DataTable EjecutarConsulta(DataTable Tabla)
        {
            if (Conector.State == ConnectionState.Open)
            {
                Conector.Close();
            }
            Adaptador = new SqlDataAdapter(Instruccion, Conector);
            Adaptador.SelectCommand.CommandTimeout = 0;
            try
            {
                Adaptador.Fill(Tabla);
            }
            catch (Exception ex)
            {
                Tabla = null;
                Errores.Add(ex);
            }
            return Tabla;
        }
        public void EjecutarTransaccion()
        {
            Comando.CommandText = Instruccion;
            Comando.Connection = Conector;
            if (Comando.Connection.State != ConnectionState.Open)
            {
                Comando.Connection.Open();
            }
            switch (TranStatus)
            {
                case 1://Iniciar
                    {
                        TranControl = Conector.BeginTransaction(TranName);
                        Comando.Transaction = TranControl;

                    }
                    break;
                case 2://Ejecutar
                    {
                        Comando.ExecuteNonQuery();
                    }
                    break;

                case 3://Completar
                    {
                        TranControl.Commit();
                        Comando.Connection.Close();
                    }
                    break;
                case 4://Deshacer
                    {
                        if (TranControl == null)
                        {
                            if (Comando.Connection.State == ConnectionState.Open)
                            {
                                Comando.Connection.Close();
                            }
                        }
                        else
                        {
                            TranControl.Rollback();
                            Comando.Connection.Close();
                        }

                    }
                    break;
            }
        }
    }
}