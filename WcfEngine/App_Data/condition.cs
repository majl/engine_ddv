﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;
using WcfEngine.Models;

namespace WcfEngine
{
    public class condition
    {
        [XmlAttribute("attribute")]
        public string attribute { get; set; }

        [XmlAttribute("operator")]
        public string operatortype { get; set; }

        [XmlAttribute("value")]
        public string value { get; set; }

        [XmlAttribute("tipo")]
        public string tipo { get; set; }

        [XmlElement("value")]
        public List<string> valuelist { get; set; }

        [XmlElement("set")]
        public List<Set> setList { get; set; }

        [XmlAttribute("entity")]
        public string entity { get; set; }

        public condition()
        {

        }
    }
}