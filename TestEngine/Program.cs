﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WcfEngine;

namespace TestEngine
{
    class Program
    {
        static void Main(string[] args)
        {
            Service1 engine = new Service1();
            //string xmlBase64 = "PD94bWwgdmVyc2lvbj0nMS4wJyA/PiAKPGVudGl0eSBuYW1lPSdDbGllbnRlJz4gCjx0YXNraWQ+ezQyQTc2NzdDLUI0RjgtRTQxMS05NjhGLTAwNTA1Njk3NzIyRn08L3Rhc2tpZD4gCjx0YXNrPlBydWViYSBtYXlvIDEyPC90YXNrPiAKPGdlbmVyYT4wPC9nZW5lcmE+IAo8Y2F0ZWdvcmlhPm51bGw8L2NhdGVnb3JpYT4gCjxmZWNoYWluaT4wMS8wMS8yMDE1PC9mZWNoYWluaT4gCjxmZWNoYWZpbj4xMi8zMS8yMDE1PC9mZWNoYWZpbj4gCjxtZWRpYT48L21lZGlhPiAKPGRkdj5QcnVlYmEgVEk8L2Rkdj4gCjxhdHRyaWJ1dGU+SWRDbGllbnRlPC9hdHRyaWJ1dGU+IAo8b3JkZXI+SWRDbGllbnRlPC9vcmRlcj4gCjxmaWx0ZXI+b3I8L2ZpbHRlcj4gCjxydWxlc29wcG9ydHVuaXR5Y29uZGl0aW9ucz4gCjxydWxlIGlkPScwJz4gCjxydWxlY29uZGl0aW9uIGlkPScwJz4gIAo8Y29uZGl0aW9uIGF0dHJpYnV0ZT0nSWRDYW5hbFJUTScgb3BlcmF0b3I9J2luJz4gCjx2YWx1ZT40PC92YWx1ZT4gCjwvY29uZGl0aW9uPiAKPGNvbmRpdGlvbiBhdHRyaWJ1dGU9J0lkUG90ZW5jaWFsJyBvcGVyYXRvcj0naW4nPiAKPHZhbHVlPjI8L3ZhbHVlPiAKPC9jb25kaXRpb24+IAo8L3J1bGVjb25kaXRpb24+ICAKPHJ1bGVjb25kaXRpb24gaWQ9JzEnPiAgCjxsaW5rLWVudGl0eSBuYW1lPSdydXRhY2xpZW50ZSc+IAo8ZnJvbT5pZGNsaWVudGU8L2Zyb20+IAo8dG8+aWRjbGllbnRlPC90bz4gCjxmaWx0ZXI+YW5kPC9maWx0ZXI+IAo8bGluay1lbnRpdHkgbmFtZT0ncnV0YSc+IAo8ZnJvbT5pZHJ1dGE8L2Zyb20+IAo8dG8+aWRydXRhPC90bz4gCjxmaWx0ZXI+YW5kPC9maWx0ZXI+IAo8Y29uZGl0aW9uIGF0dHJpYnV0ZT0nSWRDZWRpcycgb3BlcmF0b3I9J2luJz4gCjx2YWx1ZT4zMDwvdmFsdWU+IAo8L2NvbmRpdGlvbj4gCjwvbGluay1lbnRpdHk+IAo8L2xpbmstZW50aXR5PiAKPC9ydWxlY29uZGl0aW9uPiAgCjxydWxlY29uZGl0aW9uIGlkPScyJz4gIAo8bGluay1lbnRpdHkgbmFtZT0nVmVudGEnPiAKPGZyb20+SWRDbGllbnRlPC9mcm9tPiAKPHRvPklkQ2xpZW50ZTwvdG8+IAo8ZmlsdGVyPmFuZDwvZmlsdGVyPiAKPGNvbmRpdGlvbiBhdHRyaWJ1dGU9J01lcycgb3BlcmF0b3I9J3RoaXMtbW9udGgnIC8+IAo8Y29uZGl0aW9uIGF0dHJpYnV0ZT0nQ2FudGlkYWQnIG9wZXJhdG9yPSdudWxsJyB2YWx1ZT0nMCcgLz4gCjxjb25kaXRpb24gYXR0cmlidXRlPSdJZFByb2R1Y3RvJyBvcGVyYXRvcj0naW4nPiAKPHZhbHVlPjEzMTwvdmFsdWU+IAo8dmFsdWU+MTY5MDwvdmFsdWU+IAo8dmFsdWU+MTY4OTwvdmFsdWU+IAo8L2NvbmRpdGlvbj4gCjwvbGluay1lbnRpdHk+IAo8L3J1bGVjb25kaXRpb24+ICAKPC9ydWxlPiAKPC9ydWxlc29wcG9ydHVuaXR5Y29uZGl0aW9ucz4gCjxydWxlc29wcG9ydHVuaXR5bW9uaXRvcmluZz4gCjxydWxlIGlkPScwJz4gCjxydWxlY29uZGl0aW9uIGlkPScwJz4gIAo8bGluay1lbnRpdHkgbmFtZT0nVmVudGEnPiAKPGZyb20+SWRDbGllbnRlPC9mcm9tPiAKPHRvPklkQ2xpZW50ZTwvdG8+IAo8ZmlsdGVyPmFuZDwvZmlsdGVyPiAKPGNvbmRpdGlvbiBhdHRyaWJ1dGU9J01lcycgb3BlcmF0b3I9J3RoaXMtbW9udGgnIC8+IAo8Y29uZGl0aW9uIGF0dHJpYnV0ZT0nQ2FudGlkYWQnIG9wZXJhdG9yPSdtYXknIHZhbHVlPScwJyAvPiAKPGNvbmRpdGlvbiBhdHRyaWJ1dGU9J0lkUHJvZHVjdG8nIG9wZXJhdG9yPSdpbic+IAo8dmFsdWU+MTMxPC92YWx1ZT4gCjx2YWx1ZT4xNjkwPC92YWx1ZT4gCjx2YWx1ZT4xNjg5PC92YWx1ZT4gCjwvY29uZGl0aW9uPiAKPC9saW5rLWVudGl0eT4gCjwvcnVsZWNvbmRpdGlvbj4gIAo8L3J1bGU+IAo8L3J1bGVzb3Bwb3J0dW5pdHltb25pdG9yaW5nPiAKPC9lbnRpdHk+IAo=";
            //string fecRegistro = "05/19/2015 12:51";
            //string result = engine.seguimientoOportCumplida(xmlBase64, fecRegistro);
            //Console.WriteLine(result);

            string xmlBase64 = "PD94bWwgdmVyc2lvbj0nMS4wJyA/PiANCjxlbnRpdHkgbmFtZT0nQ2xpZW50ZSc+IA0KPHRhc2tpZD57RUVEQzYwM0EtQTZDMS1FNjExLTg0OEQtMDA1MDU2OUUzRTYzfTwvdGFza2lkPiANCjx0YXNrPkFncmVnYXIgUmFjayBkZSBEZXNheXVub3MgKENsaWVudGVzIEluIFRvcm5vKTwvdGFzaz4gDQo8Z2VuZXJhPjE8L2dlbmVyYT4gDQo8Y2F0ZWdvcmlhPm51bGw8L2NhdGVnb3JpYT4gDQo8ZmVjaGFpbmk+MTIvMTQvMjAxNjwvZmVjaGFpbmk+IA0KPGZlY2hhZmluPjAyLzI4LzIwMTc8L2ZlY2hhZmluPiANCjxtZWRpYT5TQU5KT1JHRS45MDE0LmpwZzwvbWVkaWE+IA0KPGRkdj5NYXJjYXIgY29tbyBjdW1wbGlkYSBsYSBvcG9ydHVuaWRhZCBjdWFuZG8gc2UgYWdyZWd1ZSBlbCByYWNrIGRlIGRlc2F5dW5vcyBjb3JyZWN0YW1lbnRlIGVqZWN1dGFkbyBlbiBlbCBhcmVhIHNlw7FhbGFkYS48L2Rkdj4gDQo8YXR0cmlidXRlPklkQ2xpZW50ZTwvYXR0cmlidXRlPiANCjxvcmRlcj5JZENsaWVudGU8L29yZGVyPiANCjxmaWx0ZXI+b3I8L2ZpbHRlcj4gDQo8cnVsZXNvcHBvcnR1bml0eWNvbmRpdGlvbnM+IA0KPHJ1bGU+IA0KPHJ1bGVjb25kaXRpb24+ICANCjxjb25kaXRpb24gYXR0cmlidXRlPSdJZENsaWVudGUnIG9wZXJhdG9yPSdpbic+IA0KPHZhbHVlPjkwMTQ8L3ZhbHVlPiANCjwvY29uZGl0aW9uPiANCjwvcnVsZWNvbmRpdGlvbj4gIA0KPC9ydWxlPg0KPC9ydWxlc29wcG9ydHVuaXR5Y29uZGl0aW9ucz4gDQo8cnVsZXNvcHBvcnR1bml0eW1vbml0b3Jpbmc+IA0KPC9ydWxlc29wcG9ydHVuaXR5bW9uaXRvcmluZz4gDQo8L2VudGl0eT4g";
            engine.GetPreview(xmlBase64);


            TestClass t = new TestClass();
            string number1 = t.method1(1, 2);
            string number2 = t.method1(2, 2, 3);
            string number3 = t.method1(1, 2 , 1);

            Console.WriteLine(number1);
            Console.WriteLine(number2);
            Console.WriteLine(number3);

            Console.ReadKey();
        }
    }

    public class TestClass
    {
        public TestClass()
        {

        }

        public string method1(int n1, int n2, int n3 = 0)
        {
            return (n1 + n2 + n3).ToString();
        }
    }
}
